"""let us solve the n-queens problem with different heuristics."""

import inf555


def default_heuristics():
    """Solve first, with the annotation corresponding to the default heuristics.

    i.e. and-parallelism (variable choice) is handled in the order of the input
    or-parallelism (value choice) is handled from the minimum available value
    to the max.
    """
    solutions = inf555.minizinc(
        "nqueens-ann.mzn",
        data={
            "n": 31,
            "search_ann": "int_search(q, input_order, indomain_min, complete)",
        }
    )
    print(solutions.statistics['solveTime'])

def all_heuristics():
    # write here
    pass

def two_best_heuristics():
    # write here
    pass
