%-*- TeX-engine: xetex; -*-
\documentclass[tikz,code,aspectratio=169]{lifewareslides}

\usepackage{chessboard}

\lstset{language=MiniZinc}

\title{Local Search \& Constraint Satisfaction Problems}

\author{Sylvain Soliman}

\date{October 7th, 2020\\
\bigskip{\scriptsize Thanks to P.\ Flener, L.\ Michel and P.\ Van Hentenryck for inspiration}}

\begin{document}

\begin{frame}[label=docker]{Docker setup}

   You can start

   {\small\lstinline|docker pull registry.gitlab.inria.fr/soliman/inf555/td3|}

   now

\end{frame}

\begin{frame}
   \titlepage%
\end{frame}

\section*{Introduction}

\begin{frame}{What is ``Local Search''}

   \begin{itemize}

      \item iterative \textbf{optimization} method

         \vfill

      \item looking for an assignment of \textcolor{inriared}{variables} to
         values of their \textcolor{inriared}{domains} that minimizes some
         \emph{cost}

         \vfill

      \item \textbf{local} move from solution to \emph{neighboring} solution

         \vfill

      \item try to (always or not) decrease the cost of the selected
         assignment

   \end{itemize}

\end{frame}

\begin{frame}{How does this relate to Constraint Solving?}

   Compared to what we have seen up to now:

   \vfill

   \begin{itemize}

      \item only \textbf{optimization}

         \vfill

      \item requires defining:

         \vfill

         \begin{itemize}

            \item \textbf{neighborhood} and

               \vfill

            \item \textbf{selection criterion} (\emph{single} state)

               \vfill

            \item \textbf{stopping criterion}

         \end{itemize}

   \end{itemize}

   \vfill

   $\Rightarrow$ \textbf{incomplete} (i.e., not optimal) but \textbf{low cost}
   (time and memory)

\end{frame}

\section*{CBLS}

% started from MaxSAT (WalkSAT, GSAT)

\begin{frame}{Constraint-Based Local Search}

   A pure CSP can be transformed easily into a LS problem:

   \pause\vfill

   Use \textbf{constraint violations} as \emph{cost function}

   \vfill

   Some constraints can be given an infinite cost, these are \emph{hard}
   constraints that all states have to satisfy

   \vfill

   Other are soft constraints that will guide the search\\
   Allows us to solve \emph{over-constrained} problems

   \vfill

   Neighborhood is defined as changing \textbf{a single variable assignment}

   \vfill

   In that framework \textcolor{inriared}{redundant constraints} play two roles:
   \pause%
   \textcolor{inriableu}{propagation} and \pause%
   \textcolor{inriaorange}{search}

\end{frame}

\begin{frame}{Violations}

   Violations defined on basic constraints

   \vfill

   Can be \textbf{composed}:

   \begin{itemize}

      \item $\cal{V}(c_1\wedge c_2) = \cal{V}(c_1) + \cal{V}(c_2)$

      \item $\cal{V}(c_1\vee c_2) = \min(\cal{V}(c_1),\cal{V}(c_2))$

         \vfill

      \item $\cal{V}(\neg c) = 1 - \min(1,\cal{V}(c))$ \pause%
         $\quad\Rightarrow$ not compatible with the above!


   \end{itemize}

   \vfill

   For global constraints, one can \emph{decompose} or not

\end{frame}

\section*{Example problem}

\begin{frame}{N-Queens as a Local Search problem}

   \lstinputlisting[linerange={6-8}]{../td8/nqueens.mzn}% chktex 8

   \vfill

   Count violations as the \textbf{total} number of identical pairs in an
   \lstinline|alldifferent| constraint

   \vfill

   Very dependent on the model! (dual, symmetries, etc.)\\
   But the state space does depend too!

   \pause\vfill

   Could use the \textbf{max} number of equalities instead of their sum\\
   try to guide the search as much as possible

\end{frame}

\begin{frame}{}

   \begin{columns}

      \begin{column}{0.4\textwidth}

         \setchessboard{maxfield=d4,largeboard,showmover=false}
         \only<1-8>{\chessboard[  % chktex 8
            setpieces={qa4,qb3,qc4,qd3},
         ]}%
         \only<9->{\chessboard[
            setpieces={qa4,qb3,qc4,qd3},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 5,
            markfields={a2,a3,c2,d4,d1},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 3,
            markfields={a1,b1,d2},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 4,
            markfields={b4,c3},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{2},
            markfields={b2,c1},
         ]}

      \end{column}

      \begin{column}{0.6\textwidth}

         \lstinline|alldifferent| lists:\\
         \smallskip
         $\rightarrow$\pause$[4,3,4,3]$ $\quad\searrow$\pause$[4,4,6,6]$
         $\quad\nearrow$\pause$[4,2,2,0]$

         \bigskip

         Violations:\pause\\
         \smallskip
         $2+$\pause$2+$\pause$1=5$

         \bigskip

         Neighborhood:\pause\\
         \smallskip
         Move \textbf{one queen} in its column\\
         i.e., change the valuation of a single variable

      \end{column}


   \end{columns}

\end{frame}

\section*{Example Local Search algorithms}

\begin{frame}{Example: Greedy Local Search (aka.\ Hill-climbing)}

   Pure \textbf{exploitation} (intensification)

   \vfill

   Analog for the discrete case of gradient descent

   \vfill

   Select the \emph{most improving} neighbor (random if multiple bests)

   \vfill

   Stop when no improvement found

\end{frame}

\begin{frame}{Example: Min. Conflict Search (MCS) / Heuristics (MCH)}

   Original heuristics for CBLS on SAT problems (and still part of GSAT,
   WalkSAT, etc.)

   \vfill

   Implemented by default in the COMET system

   \vfill

   Basis of most other heuristics

   \vfill

   Select the neighbor (i.e., variable assignment) that minimizes the
   \textbf{number} of violated constraints

   \pause\vfill

   IOW, Hill-Climbing with violations saturated at $1$

\end{frame}

\begin{frame}{Issues}

   \pause%
   Local extrema

   \vfill

   \pause%
   Plateaus

   \vfill

   \pause%
   Diagonal ridges (i.e., moves of same cost leading to different extrema)

   \vfill

   \pause%
   Big neighborhood (might require two steps:
   \textbf{variable} and then \textbf{value} selection)

\end{frame}

\begin{frame}{}

   \begin{columns}

      \begin{column}{0.33\textwidth}

         \setchessboard{maxfield=d4,largeboard,showmover=false,marginleftwidth=5pt,labelleft=false}
         \visible<1->{\chessboard[
            setpieces={qa2,qb3,qc1,qd4},
         ]}

         Violation cost = \pause\textcolor{inriared}{$1$}
      \end{column}

      \begin{column}{0.33\textwidth}

         \setchessboard{maxfield=d4,largeboard,showmover=false,marginleftwidth=5pt,labelleft=false}
         \visible<2->{\chessboard[
            setpieces={Qa2,Qb3,Qc1,Qd4},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={d3},
            coloremphstyle=\color{inriared},
            emphfields={a2,b3,c1,d4},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 2,
            markfields={a3,b4,c2,c3,d2},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 3,
            markfields={a1,a4,b1,b2,d1},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 4,
            markfields={c4},
         ]}

         Line-wise costs\pause%

      \end{column}

      \begin{column}{0.33\textwidth}

         \setchessboard{maxfield=d4,largeboard,showmover=false,marginleftwidth=5pt,labelleft=false}
         \visible<3->{\chessboard[
            setpieces={Qa2,Qb3,Qc1,Qd4},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={b4},
            coloremphstyle=\color{inriared},
            emphfields={a2,b3,c1,d4},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 2,
            markfields={a1,a3,a4,b1,d3},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 3,
            markfields={b2,c2,c3,d1,d2},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 4,
            markfields={c4},
         ]}

         Column-wise costs

      \end{column}

   \end{columns}

\end{frame}

\begin{frame}{}

   \begin{columns}

      \begin{column}{.6\textwidth}

         \setchessboard{maxfield=h8,normalboard,showmover=false}
         \chessboard[
            setpieces={Qb8,Qh7,Qc6,Qg5,Qd4,Qf3,Qa2,Qe1},
            coloremphstyle=\color{inriared},
            emphfields={b8,h7,c6,g5,d4,f3,a2,e1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={a6,h3,a3},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 2,
            markfields={c8,f8,b6,d6,e6,g6,h6,b3,d3,f1},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 3,
            markfields={d8,e8,h8,c7,d7,e7,f7,g7,f6,a5,b5,c5,f5,h5,a4,b4,c4,g4,c3,e3,g3,b2,c2,e2,h2,a1,c1,d1,g1},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 4,
            markfields={a8,g8,a7,b7,e5,f4,h4,d2,f2,g2,b1,h1},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 5,
            markfields={d5,e4},
         ]

      \end{column}

      \begin{column}{.4\textwidth}

         Line-wise costs

         \bigskip

         Real local optimum

         \bigskip

         Necessary to get through a \emph{worse} solution to get to a global
         optimum

      \end{column}

   \end{columns}

\end{frame}

\begin{frame}{Example: Random walk}

   Pure \textbf{exploration} (diversification)

   \vfill

   Select \emph{a random} neighbor

   \vfill

   Remember the best solution found

   \vfill

   Stop after a given number of iterations

\end{frame}

\section*{Better methods}

\begin{frame}{Being smarter}

   $\Rightarrow$ combine a way to escape local extrema with some
   Hill-climbing

   \vfill

   Some examples:

   \begin{itemize}

      \item diagonal moves (see \emph{Practical work session})

      \pause\item Simulated annealing (\emph{idem})

      \pause\item Tabu search

      \pause\item random \textbf{restarts} (underrated!)

   \end{itemize}

   \pause\vfill

   What if \emph{restarts were actually done simultaneously}?\\
   \textbf{population-based approaches}

\end{frame}

\pictureframe{furnace.jpg}{}

\begin{frame}{Simulated annealing}

   Inspiration from the physics' world (Metropolis-Hastings algorithm for a
   sampling states of a thermodynamic system, 1953)

   \vfill

   Allow some \emph{exploration} while the \textbf{temperature} of the system
   is high

   \vfill

   Decrease temperature with time (i.e., iterations)

   \vfill

   Focus on \emph{exploitation} when the system cools down

\end{frame}

\begin{frame}{}

   \begin{center}

      \includegraphics[height=.8\textheight]{fri_cnm_utexas_edu_SA.jpg}

   \end{center}

   \hfill{\small © Texas Materials Institute (UTexas)}

\end{frame}

\begin{frame}{Practically}

   At each step:

   \begin{itemize}

      \item select a \textcolor{inriaorange}{\textbf{random neighbor}} (no
         guidance at all\dots)

         \vfill

      \item compare its cost with the current cost

         \vfill

      \item accept it or not depending on the temperature but
         \textcolor{inriableu}{always accept improving moves}

         \vfill

      \item stop if the move was rejected and the temperature too low (return
         the best solution found)

   \end{itemize}

   \vfill

   Parameters: initial/stopping temperature, cooling regime, acceptance
   condition

\end{frame}

\begin{frame}{Parameters}

   Initial temperature: \pause%
   allow any move (\textbf{random-walk})

   \vfill

   Ending temperature: \pause%
   would reject most non-improving moves (\textbf{hill-climbing})

   \vfill

   Cooling regime: \pause%
   observed to have almost \emph{no impact}\\
   usually $T_{t+1} = (1 - c)T_t$ with a small cooling-rate, e.g., $c = 0.01$

   \vfill

   Acceptance condition: \pause%
   mostly coming from physics\\
   consensus: $\exp(\Delta/T) > r$, $\Delta$ is current cost minus new cost,\\
   $r$ is a random variable in $[0,1)$ % chktex 9

\end{frame}

\begin{frame}{Results}

   Very often used as a basic LS algorithm

   \vfill

   Decent results on the Traveling Salesman Problem\\
   not for finding an optimal solution but a \emph{good} solution

   \vfill

   On the N-Queens problem\dots\pause\\
   we'll see during the \emph{practical work session}

   \vfill

   Not always easy to fine-tune temperature

\end{frame}


\begin{frame}{Tabu search}

   Created by F.\ Glover in 1986

   \vfill

   At its core: Hill-Climbing with some kind of memory forbidding moves

   \vfill

   At each step select the \textbf{best neighbor} that is \textbf{not tabu}

   \vfill

   Stop after a given number of iterations

   \vfill

   Tabu moves force diversification \pause%
   in a more \emph{guided} way than random-walks

\end{frame}

\begin{frame}{Types of memory}

   The \emph{Tabu-list} is usually a list of recently visited states, to avoid
   cycles (classical issue with random diagonal moves)

   \vfill

   Its length is a \textbf{sensitive parameter}

   \vfill

   Sometimes not a full state but a \emph{feature} is stored

   \vfill

   in that case it might be necessary to overcome \emph{tabu} when a better
   candidate is found

   \vfill

   Intermediate-term memory (intensification) and Long-term memory
   (diversification) rules can be added

\end{frame}

\begin{frame}{}

   \begin{center}

      \setchessboard{maxfield=f6,largeboard,showmover=false}
      \only<1>{%
         \chessboard[%
            setpieces={qa6,qa5,qa4,qa3,qa2,qa1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 10,
            markfields={f5,f4,f3,f2,e4,e3},
         ]
      }%
      \only<2>{%
         \chessboard[%
            setpieces={qa6,qf5,qa4,qa3,qa2,qa1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 6,
            markfields={b6,e3,d2,e1},
         ]
      }%
      \only<3>{%
         \chessboard[%
            setpieces={qb6,qf5,qa4,qa3,qa2,qa1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 3,
            markfields={d2,e2,e1},
         ]
      }%
      \only<4>{%
         \chessboard[%
            setpieces={qb6,qf5,qa4,qa3,qd2,qa1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={c4},
         ]
      }%
      \only<5>{%
         \chessboard[%
            setpieces={qb6,qf5,qc4,qa3,qd2,qa1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={f3,d1,e1},
         ]
      }%
      \only<6>{%
         \chessboard[%
            setpieces={qb6,qf5,qc4,qf3,qd2,qa1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={e5},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={a3},
         ]
      }%
      \only<7>{%
         \chessboard[%
            setpieces={qb6,qe5,qc4,qf3,qd2,qa1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={b1},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={f5},
         ]
      }%
      \only<8>{%
         \chessboard[%
            setpieces={qb6,qe5,qc4,qf3,qd2,qb1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={a6,a4,a3},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={a1},
         ]
      }%
      \only<9>{%
         \chessboard[%
            setpieces={qa6,qe5,qc4,qf3,qd2,qb1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={a4},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={b6},
         ]
      }%
      \only<10>{%
         \chessboard[%
            setpieces={qa6,qe5,qa4,qf3,qd2,qb1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={b6,e6,c5},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={c4},
         ]
      }%
      \only<11>{%
         \chessboard[%
            setpieces={qb6,qe5,qa4,qf3,qd2,qb1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 1,
            markfields={e6,c4,c1,f1},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={a6},
         ]
      }%
      \only<12>{%
         \chessboard[%
            setpieces={qe6,qe5,qa4,qf3,qd2,qb1},
            pgfstyle={[base,at={\pgfpoint{0pt}{-0.4ex}}]text},
            text=\fontsize{1.2ex}{1.2ex}\bfseries 0,
            markfields={c5},
            text=\fontsize{1.2ex}{1.2ex}\bfseries\textcolor{inriared}{1},
            markfields={a6,b6},
         ]
      }%
      \only<13>{%
         \chessboard[%
            setpieces={qe6,qc5,qa4,qf3,qd2,qb1},
         ]
      }%

   \end{center}



\end{frame}
\begin{frame}{Results}

   Same as SA but better/worse

   \vfill

   More \emph{guided}, but more parameters

   \vfill

   Good results on TSP

   \vfill

   Even trickier to fine-tune (especially complex Tabu structures/rules)

\end{frame}

\begin{frame}{Population-based approaches}

   Many versions:
   Genetic algorithms, Particle Swarm Optimization, Ant-Colony Optimization,
   etc.

   \vfill

   Main idea:
   instead of restarts, use the information of parallel runs \emph{while} they
   are running

   \vfill

   Balance between exploration/exploitation, diversification/intensification
   remains hard

\end{frame}

\section*{Conclusion}

\begin{frame}{Conclusion}

   Incomplete but \textbf{efficient} method, even with \emph{simple
   algorithms} (e.g., Hill-climbing with restarts and diagonal moves)

   \vfill

   Used for \textbf{hard} problems for which a complete search is not
   tractable (e.g., Ant Colony on graph problems by C.\ Solnon)

   \vfill

   Or for problems that are \textbf{over-constrained} (and can be
   expressed as optimization) (e.g., MaxSAT)

   \vfill

   Can be made generic for CSPs once violations are defined

   \vfill

   Remains often tricky to parametrize\\
   Does \textbf{never prove optimality}

\end{frame}

% \begin{frame}{Previously on INF555\dots}

%    \begin{enumerate}

%       \item Introduction to \textcolor{inriaorange}{\textbf{CSPs}} and to the \textcolor{inriaorange}{\textbf{modeling}}
%          language MiniZinc

%          \vfill

%       \item Boolean satisfiability, \textcolor{inriaorange}{\textbf{SAT}} solvers

%          \vfill

%       \item Polynomial complexity classes in SAT, phase transitions in random k-SAT

%          \vfill

%       \item Constraint propagation and \textcolor{inriaorange}{\textbf{domain filtering}} algorithms

%          \vfill

%       \item Search and \textcolor{inriaorange}{\textbf{heuristics}}

%          \vfill

%       \item Global constraints

%          \vfill

%       \item Symmetries

%          \vfill

%       \item \textcolor{inriaorange}{\textbf{Arithmetic}} Constraints

%          \vfill

%       \item Constraint-Based Local Search

%    \end{enumerate}

% \end{frame}

% \begin{frame}{Take-home message}

%    \textbf{Holy grail:} \emph{Programming = Modeling}\pause\\
%    \hfill$\Rightarrow$ actually not so far\dots

%    \pause\vfill

%    many available solvers efficient on many hard problems

%    \vfill

%    even if none can be efficient on all (unless $P=NP$)

%    \vfill

%    Keep CP in mind and you'll see \textbf{problems you can solve everywhere!}

%    \pause\vfill

%    But be careful:\\ \emph{``I suppose it is tempting, if the only tool you
%    have is a hammer, to treat everything as if it were a nail.''} --- A.\
%    Maslow

% \end{frame}

\againframe{docker}

\end{document} % chktex 17
