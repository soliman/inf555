"""Generic module for using pymzn nicely in a Jupyter notebook.

Used in the class INF555
Written by Sylvain.Soliman@inria.fr
"""
import nest_asyncio

import matplotlib.pyplot as plt
import numpy as np

from minizinc import Instance, Model, Solver, result


nest_asyncio.apply()

# Exports
gecode = Solver.lookup("gecode")
chuffed = Solver.lookup("chuffed")
cbc = Solver.lookup("cbc")
Status = result.Status


def minizinc(*files, solver=gecode, data={}, all_solutions=False):
    """Solve using minizinc."""
    model = Model(files[0])
    for dzns in files[1:]:
        model.add_file(dzns, parse_data=False)
    instance = Instance(solver, model)
    for key, value in data.items():
        if isinstance(value, str):
            instance.add_string(f'{key} = {value};\n')
        else:
            instance[key] = value
    return instance.solve(all_solutions=all_solutions)
