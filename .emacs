(add-to-list 'load-path "~/minizinc-mode")
(require 'minizinc-mode)
(add-to-list 'auto-mode-alist '("\\.mzn\\'" . minizinc-mode))
