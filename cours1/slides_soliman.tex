%-*- TeX-engine: xetex; -*-
% !TeX program = xelatex
\documentclass[tikz,code,aspectratio=169]{lifewareslides}

\title{Constraint-based Modeling and Algorithms for Decision-making --- INF555}

\author{Sylvain Soliman}

\date{September 23rd, 2020}

\lstset{language=MiniZinc}

\DeclareMathOperator*{\argmin}{argmin}

\begin{document}

\begin{frame}
   \titlepage%
\end{frame}

\hpictureframe{turing.jpg}{%
   \textcolor{black}{%
      \large
      François Fages, Sylvain Soliman\\
      Project-team Lifeware --- Inria Saclay\\
      \url{http://lifeware.inria.fr/}
   }
   \vspace*{6cm}
}

\part{Decision problems, optimization, complexity and modelling}

\frame{\partpage}

\begin{frame}{Decision Problems}

   \begin{columns}
      \column{.4\textwidth}
      \begin{itemize}

         \item Finite input

            \begin{itemize}
               \item Words
               \item Rational numbers
               \item Images
               \item Sounds
               \item Programs
               \item \dots
            \end{itemize}

      \end{itemize}
      \column{.1\textwidth}
      \ 
      \column{.4\textwidth}
      \includegraphics[width=\textwidth]{colored_australia}
   \end{columns}
      \vfill

   \begin{itemize}

     \item \textbf{yes/no} output 

        \begin{itemize}
           \item providing an arbitrary solution is optional
           \item providing \emph{all solutions} is another class:\\
              \textbf{enumeration problem}
        \end{itemize}

   \end{itemize}
\end{frame}

\begin{frame}{Examples}

   We will see during the class:

   \begin{itemize}
      \item Can we place $N$ queens on a chessboard with no attack?
      \item Do we have enough rooms for the lectures?
         \textbf{assignment problem}
      \item Can we land in the next 20 mn a given set of flights arriving in Orly?
         \textbf{scheduling}
      \item Can we find a sequence of actions to achieve a given goal?
         \textbf{planning}
      \item \dots
   \end{itemize}

   \vfill

   Other classical examples include \textbf{routing} (traveling salesman),
   \textbf{personnel staffing}, etc.

\end{frame}

\begin{frame}{Optimization Problems}

   \begin{columns}
      \column{.45\textwidth}
      \begin{itemize}

         \item Input: finite data

            same as before

      \end{itemize}
      \column{.5\textwidth}
      \includegraphics[width=\textwidth]{jobshop}
   \end{columns}

      \vfill

   \begin{itemize}
         \item output: \textbf{optimal cost}

            \begin{itemize}
               \item single number, or
               \item vector of numbers for \emph{multi-objective}
                  optimization

                  \vfill

               \item providing an arbitrary optimal solution is optional
               \item providing \emph{all solutions} is another class:\\
                  \textbf{enumeration problem}
            \end{itemize}

   \end{itemize}

\end{frame}

\begin{frame}{}

   \begin{itemize}
      \item How many rooms are necessary to give the lectures?
      \item What is the time required to land a given set of flights arriving in Orly?
      \item How many flights from a given set of flights can land in the
         next 20min?
      \item \dots
   \end{itemize}

   \vfill

   An optimization problem admits one \textbf{associated decision problem}:
   Is there a solution of given cost $k$?

   \vfill

   Several optimization problems can be associated to a decision
   problem (choice of the inputs treated as objective function)

\end{frame}

\begin{frame}{Mono-objective vs. Multi-objective optimization}

   The cost is always finite data

   \vfill

   An \emph{unique} rational number for mono-objective optimization.

   \begin{itemize}
      \item single optimization criterion
      \item possibly a robustness criterion w.r.t.\ perturbations
      \item the aggregation of multiple criteria representing a
         \textbf{trade-off}
   \end{itemize}

\end{frame}
\begin{frame}{Mono-objective vs. Multi-objective optimization}

   Some finite vector of rational numbers for multi-objective optimization

   \[(f,g)<(f',g') \triangleq (f<f′\wedge g\leq g')\vee(f\leq f'\wedge g<g')\]

   \vfill

   The optimal cost vectors $\max(f,g)$ not unique:\\
   \textbf{Pareto frontier}

\end{frame}

\begin{frame}{Pareto frontier}
   % TODO potato + peanut figure asking to find the frontier, what would a
   % figure with a singleton Pareto front look like?

\end{frame}

\begin{frame}{Computational Time-complexity Classes}

   An algorithm belongs to time-complexity class $C$ if it requires \textbf{at
   most} $C(n)$ elementary operations on a
   random access machine for an \textbf{input of size $n$}

   \vfill

   A problem belongs to class $C$\\
   if there exists an algorithm in $C$ to solve it

   \vfill

   \(g(n)\in\cal{O}(f(n))\triangleq\exists k, n_0\quad \forall n>n_0\quad
   g(n)\leq k\cdot f(n)\)

\end{frame}

\begin{frame}{Examples}
   \begin{tabular}{ll}
       $\cal{O}(1)$ & constant time, independent of the input\\
       $\cal{O}(n)$ & linear time\\
       $\cal{O}(n\log n)$ & best \textbf{possible} complexity for sorting\\
       $\cal{O}(n^2)$ & quadratic time\\
       P / PTIME & \textbf{polynomial time}, i.e. $\cal{O}(n^k)$ for some $k$\\
       $\cal{O}(2^n)$ &\\
       EXPTIME & exponential time, i.e. $\cal{O}(k^n)$ for some $k$\\
       $\cal{O}(2^{2^n})$ &\\
       Non-elem. & not bounded by a finite tower of exponentials $\cal{O}(2^{2^{2^{\dots}}})$\\
   \end{tabular}
\end{frame}

\begin{frame}{Non-deterministic Time-complexity}

   \textbf{NP}: class of languages recognized in polynomial time by a
   \textbf{non-deterministic} Turing machine

   $\equiv$ decision problems with proofs \textbf{verifiable in PTIME}

   \vfill

   E.g., disjunctive scheduling, timetabling, planning, \dots

   \vfill
   \vfill

   \textbf{NP-complete}: class of problems \textbf{in NP} that can encode in
   polynomial time \textbf{any other NP problem},
   i.e., the \emph{hardest} NP problems

   \vfill

   E.g., boolean satisfiability, graph coloring, disjunctive scheduling, \dots

\end{frame}

\begin{frame}{}

   \textbf{NP-hard}: class of problems \emph{harder than NP}, i.e.,
   they can encode in polynomial time any NP problem

   \begin{center}
      \includegraphics[width=.6\textwidth]{P_vs_NP}
   \end{center}

   E.g., \emph{optimization problems} with NP-complete associated decision problem

\end{frame}

\begin{frame}{Space-complexity Classes}

   An algorithm belongs to a space complexity class $C$ if it requires
   \textbf{at most} $C$ memory locations

   PSPACE:\@ polynomial space, i.e., $\cal{O}(n^k)$ for some $k$

   \vfill

   Why do we have NP$\subset$PSPACE?\@

   \pause%

   \vfill
   \textbf{Iterative deepening}!

   Bounded backtracking with increasing depth $d$

   At depth $d$, $d$ binary choices to remember, $\cal{O}(d)$ space

   Computation of result at $\cal{O}(n^k)$ depth: $\cal{O}(n^k)$ space

\end{frame}

\begin{frame}{Size of the Search Space $\not =$ Time Complexity}

   Size of the solution domain:\\
   \textbf{bad upper bound for time-complexity}

   \vfill

   \begin{tabular}{lll}
      \toprule
      Sorting $n$ integers & $!n$ & $\cal{O}(n\log n)$\\
      Placing $n$ queens & $n^n$ or $!n$ & \pause$\cal{O}(1)$ analytic solution\\
      Fermat Theorem  & $\infty$ & $\cal{O}(1)$\\
      $\forall n\exists? a b c\ a^n+b^n=c^n$ & & \pause$n>\!\!?\ 2$ (A.\ Wiles 1994)\\
      \bottomrule
   \end{tabular}

   \vfill

   Separation results (like $\mathbf{P\not =NP}$) are hard:\\
   need to quantify on all mathematical properties

\end{frame}

\begin{frame}{Theoretical Complexity only Bounds the Practical Complexity}

   \textbf{Worst-case} complexity $\not =$ \textbf{average} time-complexity
   (random inputs)

   \vfill

   Can we solve NP-hard problems on very large instances?\\
   \pause%
   \textbf{Yes!} But not on all (even small) instances (probably exponential)

   \vfill

   Practical instances may happen to be easy\\
   --- \emph{Polynomial class}\\
   --- \emph{Phase transition}\\
   --- \emph{Pathological examples} (exponential algorithm with polynomial
   empirical complexity)

\end{frame}

\begin{frame}{Constraint Satisfaction Problems}

   \textcolor{green}{Input:}

   \begin{tabular}{ll}
      Finite set of \textbf{Variables} & $x_1,\dots,x_n$\\

      Corresponding \textbf{Domains} of values & $D_1,\dots,D_n$\\

      Finite set of \textbf{Constraints} & $c_1,\dots,c_k$\\

      Optional objective function & $f(x_1,\dots,x_n)\in\mathbb{R}$
   \end{tabular}

   \vfill

   \textcolor{inriared}{Output:}

   \begin{tabular}{ll}
      Decision &
      $\exists? (x_1,\dots,x_n)\in D_1\times\dots\times D_n$\\
               & s.t.\ $c_1\wedge\dots\wedge c_k$\\

      Optimization &
      $\displaystyle\min_{c_1\wedge\dots\wedge c_k}f(x_1,\dots,x_n)$\\

      Solutions &
      $\displaystyle\argmin_{c_1\wedge\dots\wedge c_k}f(x_1,\dots,x_n)$
   \end{tabular}

\end{frame}

\begin{frame}{The $N$ Queens Problem}

   $N$ Queens on an $N\times N$ chessboard with no attack

   \pause\vfill

   \begin{columns}
      \column{.6\textwidth}
      \textbf{Variables:} \pause$x_1,\dots,x_N$ (columns)

      \pause\vfill
      \textbf{Domains:} \pause$D_i=\{1,\dots,N\}$ (lines)

      \pause\vfill
      \textbf{Constraints:} \pause``not same line''
      \pause$\forall i<j\ x_i\not =x_j$

      \pause\vfill
      ``not same diagonal'' $\forall i<j\ x_i\not =x_j+i-j\wedge x_i\not
      =x_j-i+j$

      \pause\column{.4\textwidth}
      \includegraphics[width=\textwidth]{125reines}
   \end{columns}

\end{frame}

\begin{frame}{MiniZinc Constraint Modelling Language}

   \lstinputlisting[basicstyle=\small\ttfamily,lastline=10]{nqueens.mzn}

\end{frame}

\begin{frame}{(Declarative) Programming as Modeling}

   \begin{itemize}
      \item[1940] Machine language
      \item[1954] Fortran: arithmetic expressions and control flow
      \item[1959] Lisp: functions over lists (Church’s $\lambda$-calculus)
      \item[1960] Algol: algorithms
      \item[1970] C\@: for whole operating system
   \item[1972] \textbf{Prolog}: first-order logic
      \item[1975] Smalltalk: objects
      \item[1978] ML\@: typed functions
      \item[1984] \textbf{Constraint Logic Programming}
      \item[1990] Constraint programming libraries for C++
      \item[1991] Python
      \item[1996] Java: object-oriented threaded programming
      \item[2008] \textbf{Zinc}: solver-independent constraint language
      \item[\dots]
   \end{itemize}

\end{frame}

\begin{frame}{Von Neumann vs.\ Constraint machine}

   \noindent%
   \begin{tikzpicture}[scale=0.9]
      \node [align=center]at (1.5, 7) {memory of values\\programming variables};
      \node [align=center]at (8.5, 7) {memory of constraints\\mathematical variables};

      \draw (0.5, 0) rectangle (2, 6);
      \draw (0.5, 2) -- (2, 2);
      \draw (0.5, 1.6) -- (2, 1.6);
      \draw (0.5, 3) -- (2, 3);
      \draw (0.5, 3.4) -- (2, 3.4);
      \draw (0.5, 5.6) -- (2, 5.6);

      \node at (0, 5.8) {$V_1$};
      \node at (0, 3.2) {$V_i$};
      \node (vi) at (2, 3.2) {};
      \node at (0, 1.8) {$V_j$};
      \node (vj) at (2, 1.8) {};
      \node (assign) at (4, 2.5) {$V_i \leftarrow V_j + 1$};

      \draw [->] (assign) -- node [sloped, anchor=center, above] {write} (vi);
      \draw [->] (vj) -- node [sloped, anchor=center, below] {read} (assign);

      \draw plot [smooth cycle, tension=0.7] coordinates 
         {(6.3, 4.2) (8.5, 4.5) (9.2, 3.5) (10, 2) (9, 1.4) (5.7, 1.5)};

      \node (store) [align=center] at (7.6, 2.8)
         {$X_i\in [3,15]$\\$\sum a_i X_i\geq b$\\\texttt{card(1,}\\
         $\quad [X\geq Y+5, \dots])$};

      \node (add) at (10, 5) {$X_i = X_j + 2$};
      \node (test) at (10.5, 0.5) {$X_i\geq 5$?};

      \draw [->] (add) -- node [sloped, anchor=center, below] {add} (store);
      \draw [->] (store) -- node [sloped, anchor=center, above] {test} (test);

      \node [align=center] at (8.5, -0.5) {and-concurrency\\or-parallelism};
   \end{tikzpicture}

\end{frame}

\begin{frame}{Constraints are Domain Filtering Agents}

   \begin{columns}
      \column{.55\textwidth}

      for each constraint $c_i$

      \smallskip

      for each variable $x_j$ in $c_i$

      \smallskip

      compute the projection $P_{ij}$ of the solutions on $x_j$

      \smallskip

      over-approximate if necessary

      \smallskip

      $D_j\gets D_j\cap P_{ij}$

      \column{.4\textwidth}
      \includegraphics{arc}

   \end{columns}

   \vfill

   communication through shared variables in $\wedge$

   \vfill 

   Or-branches:
   no communication, easy to parallelize

   And-concurrency:
   lots of comm., difficult to parallelize

\end{frame}

\part{MiniZinc, Jupyter and friends\dots}

\frame{\partpage}

\section*{Introduction}

\begin{frame}{MiniZinc}

   Most of the course will use MiniZinc as programming/modelling language

   \begin{center}

      \begin{tikzpicture}[node distance=4cm, thick]
      
         \node[text width=3cm, align=center] (mini) {MiniZinc\\high-level};
         \node[text width=3cm, align=center, right of=mini] (flat)
            {FlatZinc\\low-level};
            \node[left of=mini] {\includegraphics[height=3cm]{MiniZn_logo_2.png}};
      
      
         \draw[->] (mini) -- node[above] {\small compile} (flat);
      
      \end{tikzpicture}

   \end{center}

\end{frame}

\begin{frame}[fragile=singleslide]{MiniZinc syntax}

   A MiniZinc program may contain \textbf{parameters}

   \vfill

   \begin{lstlisting}
int: i = 1;
int j;
j = 2;
   \end{lstlisting}

   \vfill

   Fixed value (named constants), of type \lstinline|int, float, bool| or
   \lstinline|string|

   \vfill

   May be given in a separate \emph{data} file (\lstinline|.dzn|)

\end{frame}

\begin{frame}[fragile=singleslide]{MiniZinc syntax}

   A MiniZinc program may contain \textbf{decision variables}

   \vfill

   \begin{lstlisting}
var int: u;
var 1..10: v;
   \end{lstlisting}

   \vfill

   Only \lstinline|int| or \lstinline|float|, given with an optional
   \emph{domain}

   \vfill

   Parameters and variables can appear in \textbf{constraints} (using the usual
   arithmetic and Boolean relation operators)

   \vfill

   \begin{lstlisting}
constraint u = 18 * v + 42;
constraint alldifferent([x, y, z]);
   \end{lstlisting}

\end{frame}

\begin{frame}[fragile=singleslide]{Additional MiniZinc Syntax}

   It is possible to define \textbf{sets} and \textbf{arrays} of objects

   \vfill

   \begin{lstlisting}
set of int: STUDENT = 0..n;
% m[i] is the mark of student number i
array[STUDENT] of var int: m;
   \end{lstlisting}

   \vfill

   Iterators over those structures are given

   \vfill

   \begin{lstlisting}
constraint exists(s in STUDENT)
   (m[s] = 20);
constraint forall(s in STUDENT)
   (m[s] <= 20);
   \end{lstlisting}

\end{frame}

\begin{frame}[fragile=singleslide]{Final Bits}

   At least one \textbf{solve} statement must appear in a MiniZinc model

   \vfill

   \begin{lstlisting}
solve satisfy;
solve maximize u+3*v;
solve minimize sum(i in STUDENT)(m[i]);
   \end{lstlisting}

   \vfill

   \textbf{output} takes a list of strings and displays them

   \vfill

   Many other things (function or predicate definition, enums,
   comprehensions, etc.) $\longrightarrow$ in TDs

\end{frame}

\begin{frame}{Jupyter}

   Work will be done using \textbf{Jupyter Notebooks}

   \vfill

   Assuming some basic knowledge of Python

   \vfill

   Otherwise see:
   \url{https://docs.python.org/3/tutorial/introduction.html}

   \vfill

   Code editing will use a version of \textbf{VS Code} embedded in a browser.
   (\texttt{vim} and \texttt{emacs} are also provided)

\end{frame}

\hpictureframe{Jupyter_browser.png}{}
\hpictureframe{Jupyter_notebook.png}{}
\hpictureframe{code_server.png}{}

\section*{Setup}

\begin{frame}{Docker}

   All will be run from \textbf{Docker} containers

   \vfill

   Same environment for everyone

   \vfill

   \textbf{Do not forget to save your work!}

   upload it to the Moodle at the end of the TD session (and later\dots)

\end{frame}
\begin{frame}[fragile=singleslide]{Setup}

   \begin{enumerate}

      \item Download Docker \url{https://docs.docker.com/install/}

         \vfill

      \item Install Docker

         \vfill

      \item Pull the image for the course

   \end{enumerate}

   {\small
         \begin{verbatim}
      docker pull \
      registry.gitlab.inria.fr/soliman/inf555
         \end{verbatim}
   }

\end{frame}

\begin{frame}{Windows Users}

   You might need Docker Toolbox (and not CE) if you are using \emph{Family
   Edition}\\
   \url{https://docs.docker.com/toolbox/toolbox_install_windows/}

   \vfill

   You might need to use \texttt{192.168.99.100} instead of \texttt{localhost}
   for connecting to Jupyter

   \vfill

   If all else fails, use VirtualBox to install an Ubuntu image and follow the
   instructions to install Docker there

\end{frame}

\begin{frame}[fragile=singleslide]{TD1}

  Join the DIX's Slack \url{https://join.slack.com/t/dix-polytechnique/signup} and its \textbf{\#inf555} channel.

  \vfill

  It will be our main communication tool during the practical session.

  I will answer using Slack's threads, direct channels, and if needed Zoom screen sharing/discussion

  \vfill

  Now pull the missing files

   {\small
      \begin{verbatim}
docker pull \
registry.gitlab.inria.fr/soliman/inf555/td1
      \end{verbatim}
   }

\end{frame}

\begin{frame}[fragile=singleslide]{TD1}

   Run on \textbf{local port 8888} with the \texttt{work} directory
   of the container pointing to where you launch the command

   {\small
      \begin{verbatim}
docker run -p 8888:8888  -p 8080:8080 -v \
"$PWD":/home/jovyan/work \
registry.gitlab.inria.fr/soliman/inf555/td1
      \end{verbatim}
   }

   \vfill

   You can now start the TD\@:
   \url{http://localhost:8888/notebooks/TD.ipynb}
\end{frame}

\end{document}


