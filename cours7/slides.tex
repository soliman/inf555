%-*- TeX-engine: xetex; -*-
% !TeX program = xelatex
\documentclass[tikz,code,aspectratio=169]{lifewareslides}

\usetikzlibrary{matrix,fit}

\title{Global Constraints}

\author{Sylvain Soliman}

\date{November 18th, 2020\\
\bigskip{\scriptsize Thanks to J.-C.\ Régin, P.\ van Hentenryck and
C.\ Bessiere for inspiration\\ and to C.\ Berge for genius}}

\begin{document}

\begin{frame}[label=docker]{Docker setup}

   You can start

   \texttt{\small docker pull \textbackslash\\
   registry.gitlab.inria.fr/soliman/inf555/td7}

   now

\end{frame}

\begin{frame}
   \titlepage%
\end{frame}

\section*{Introduction}

\hpictureframe{EngageGlobal.jpg}{}

\begin{frame}{What's a global constraint?}

   Constraints that can involve any number $n$ of
   variables (i.e., not only binary)

   \vfill

   Complex relations between variables, \textbf{useful} in applications (e.g.\
   \visible<2>{\texttt{alldifferent}})

   \vfill

   With better/\textbf{more powerful} propagation than binary constraints
   (solved open problems about sport scheduling)

   \vfill

   Which require \emph{ad-hoc} AC algorithms (otherwise $\lvert
   D\rvert^n$)

\end{frame}

\begin{frame}{Arc-consistency (Domain-consistency)}

   Obviously

   \vfill

   $x_1\not=x_2\wedge x_2\not=x_3\wedge x_3\not=x_1$\\
   is \textbf{arc-consistent} on the domains $\{0, 1\}$

   \vfill
   \vfill

   whereas

   \vfill

   \texttt{alldifferent}$(x_1,x_2,x_3)$\\
   is \textbf{not}

\end{frame}

\pictureframe{crash-and-burn}{\textcolor{white}{\Large Demo}}

\section*{Examples}

\hpictureframe{scaramanga_boholeatherbackpacklarge.jpg}{\large Knapsack?}

\begin{frame}{The knapsack problem}

   \textbf{Wikipedia:}

   \begin{quote}

   The knapsack problem has been studied for more than a century, with early
   works dating as far back as 1897. The name \emph{knapsack problem} dates
   back to the early works of mathematician Tobias Dantzig (1884–1956), and
   refers to the commonplace problem of packing the most valuable or useful
   items without overloading the luggage.

   \end{quote}

\end{frame}

\begin{frame}{A knapsack global constraint}

   \begin{itemize}

      \item \textbf{Original optimization problem:} Given $n$ items of weights
         $w_i$ and value $v_i$ fit as much value as possible in a knapsack of
         capacity $S$.

         \vfill

      \item \textbf{Derived decision problem:} Given $n$ items of weights
         $w_i$ can we chose some of them ($x_i$) such that they fit in a
         knapsack with bounded capacity.

         \(\displaystyle l\leq\sum_{i=1}^{n}w_i x_i\leq U\)

         Global constraint on the variables \pause%
         \(x_i\in\{0, 1\}\)

   \end{itemize}

\end{frame}

\begin{frame}{How can we filter?}

   Brute force enumeration is not great:
   decision is \textbf{NP-complete}

   \vfill

   Take inspiration from the optimization problem

   \vfill

   \textbf{Pseudo-polynomial} dynamic algorithm in $\cal{O}(nU)$:

   build a graph (\emph{forward phase}) and find a shortest path in it

   \vfill

   We keep a (simplified) forward phase, but add a backward phase to remove
   paths incompatible with the $[l, U]$ constraint.

   Then prune impossible values from the domain.

\end{frame}

\tikzset{
   good/.style={alt=#1{fill=inriavert!30}{}}
}

\begin{frame}{Example: $10\leq 2x_1 + 3x_2 + 4x_3 + 5x_4\leq12$}

   \begin{center}
      \begin{tikzpicture}[thick]
         \matrix (m) [
            matrix of math nodes,
            row sep=9mm,
            column sep=0,
            nodes={
               draw, minimum size=7mm, text width=8mm,
               inner sep=0pt, align=center, font=\small,
               text height=1.5ex, text depth=.25ex},
            nodes in empty cells,
         ] {
            |[good=<10->]| 0  \&  \&  \&  \&  \&  \&  \&  \&  \&  \&  \&  \& \\
            |[good=<9->]| 0  \&  \& |[good=<9->]| 2 \&  \&  \&  \&  \&  \&  \&  \&
               \&  \& \\
            0  \&  \& |[good=<8->]| 2 \& |[good=<8->]| 3 \&  \& |[good=<8->]| 5 \&  \&
               \&  \&  \&  \&  \& \\
            0  \&   \& 2 \& 3 \& 4 \& |[good=<7->]| 5 \& |[good=<7->]| 6 \& 
               |[good=<7->]| 7 \&   \& 9 \&   \&   \& \\
            0  \&   \& 2 \& 3 \& 4 \& 5 \& 6 \& 7 \& 8 \& 9 \&
            |[good=<6->]| 10 \& |[good=<6->]| 11 \& |[good=<6->]| 12 \\
         };

         \xdef\values{1}
         \foreach \level in {1,...,4}
         {
            \pgfmathtruncatemacro{\nextlevel}{\level + 1}
            \xdef\newvalues{\values}
            \foreach \v in \values
            {
               \pgfmathtruncatemacro{\take}{\v + \level + 1}
               \ifnum\take<14
                  \draw[->,inriableu,visible on=<\nextlevel->]
                     (m-\level-\v.south) --(m-\nextlevel-\take.north);
                  \xdef\newvalues{\newvalues, \take}
               \fi
               \pgfmathparse{\v == 1 ? "$x_\level$" : ""}
               \draw[->,inriared,visible on=<\nextlevel->]
                  (m-\level-\v) -- node[black,left] {\pgfmathresult}
                  (m-\nextlevel-\v);
            }
            \xdef\values{\newvalues}
         }

         \foreach \level in {5,...,3}
         {
            \ifnum\level=5
               \xdef\badvalues{1,3,4,5,6,7,8,9,10}
            \else\ifnum\level=4
               \xdef\badvalues{1,3,4,5,10}
               \else
                  \xdef\badvalues{1}
               \fi
            \fi
            \pgfmathtruncatemacro{\prevlevel}{\level - 1}
            \pgfmathtruncatemacro{\slide}{12 - \level}
            \foreach \v in \badvalues
            {
               \pgfmathtruncatemacro{\taken}{\v - \level}
               \draw[->,white,densely dashed,visible on=<\slide->]
                  (m-\prevlevel-\v) -- (m-\level-\v);
               \ifnum\taken>0
                  \draw[->,white,densely dashed,visible on=<\slide->]
                     (m-\prevlevel-\taken.south) -- (m-\level-\v.north);
               \fi
            }
         }

      \end{tikzpicture}
   \end{center}

\end{frame}

\begin{frame}{Satisfiability $\Rightarrow$ filtering}

   In the previous example we obtain $x_4 = 1$ in all feasible solutions

   \vfill

   this can be propagated and maintained incrementally:\\ \medskip
   squash one level of the graph, and if necessary shift right the levels
   above

   \vfill

   We deduce an AC algorithm from one that computes efficiently (and if
   possible incrementally), not one, but \textbf{all feasible solutions}

\end{frame}

\begin{frame}{Alldifferent}

   \textbf{Global Constraint Catalog:}

   \begin{quote}
      Denotes the fact that we have one or several cliques of disequalities.
   \end{quote}

   \vfill

   Example:

   \begin{align*}
      D(x_1)&=\{1,2\} & D(x_2)&=\{2,3\} & D(x_3)&=\{1,3\}\\
      D(x_4)&=\{2,4\} & D(x_5)&=\{3,4,5,6\} & D(x_6)&=\{6,7\}
   \end{align*}

\end{frame}

\begin{frame}{Maximum Matching}

   \textbf{Wikipedia:}

   \begin{quote}

      Given a graph $G = (V,E)$, a \emph{matching} $M$ in $G$ is a set of
      pairwise non-adjacent edges

      [\dots]

      A \emph{maximum matching} is a matching that contains the largest
      possible number of edges

   \end{quote}

   \vfill

   Now associate a vertex to each variable, one to each value
   and an edge between $x$ and $v$ if $v\in D(x)$

   \vfill

   If the size of the matching is equal to the number of variables, it
   represents a solution to the \texttt{alldifferent} constraint

\end{frame}


\tikzset{
   >=Computer Modern Rightarrow,
   take/.style={alt=#1{inriableu}{}},
   goodarc/.style={alt=#1{inriavert}{}},
   bad/.style={alt=#1{inriared}{}},
   arc/.style={line width=3pt, gray},
   matrixnode/.style={%
      draw, minimum size=8mm, text width=8mm,
      inner sep=0pt, align=center,
      text height=1.5ex, text depth=.5ex,
   },
   alldiff/.pic={%
      \matrix[
         matrix of math nodes,
         row sep=3cm,
         nodes={matrixnode},
      ]{
         \& |(x1)| x_1 \& \& |(x2)| x_2 \& \& |(x3)| x_3 \& \& %  chktex 36
            |(x4)| x_4 \& \& |(x5)| x_5 \& \& |(x6)| x_6 \& \\ %  chktex 36
         |(v1)| 1 \& \& |(v2)| 2 \& \& |(v3)| 3 \& \& |(v4)| 4 \& %  chktex 36
            \& |(v5)| 5 \& \& |(v6)| 6 \& \& |(v7)| 7\\  %  chktex 36
      };

      \draw[arc] (x1) -- (v1);   %  chktex 8
      \draw[arc] (x1) -- (v2);   %  chktex 8

      \draw[arc] (x2) -- (v2);   %  chktex 8
      \draw[arc] (x2) -- (v3);   %  chktex 8

      \draw[arc] (x3) -- (v1);   %  chktex 8
      \draw[arc] (x3) -- (v3);   %  chktex 8

      \draw[arc] (x4) -- (v2);   %  chktex 8
      \draw[arc] (x4) -- (v4);   %  chktex 8

      \draw[arc] (x5) -- (v3);   %  chktex 8
      \draw[arc] (x5) -- (v4);   %  chktex 8
      \draw[arc] (x5) -- (v5);   %  chktex 8
      \draw[arc] (x5) -- (v6);   %  chktex 8

      \draw[arc] (x6) -- (v6);   %  chktex 8
      \draw[arc] (x6) -- (v7);   %  chktex 8
   }
}
\begin{frame}{Graph constraint for Alldifferent}

   \begin{align*}
      D(x_1)&=\{1,2\} & D(x_2)&=\{2,3\} & D(x_3)&=\{1,3\}\\
      D(x_4)&=\{2,4\} & D(x_5)&=\{3,4,5,6\} & D(x_6)&=\{6,7\}
   \end{align*}

   \begin{center}
      \begin{tikzpicture}[thick]

         \pic{alldiff};

         \draw[arc,take=<2>] (x1) -- (v1);

         \draw[arc,take=<2>] (x2) -- (v2);

         \draw[arc,take=<2>] (x3) -- (v3);

         \draw[arc,take=<2>] (x4) -- (v4);

         \draw[arc,take=<2>] (x5) -- (v5);

         \draw[arc,take=<2>] (x6) -- (v6);

      \end{tikzpicture}
   \end{center}
\end{frame}

\begin{frame}{General GC filtering strategy}

   \pause%

   Find an efficient algorithm for checking satisfiability

   \pause\vfill

   make it efficient for \emph{all} solutions

   \pause\vfill

   obtain arc-consistency/domain-consistency

\end{frame}

\begin{frame}{How to find a maximum matching?}

   Iteratively \textbf{improve} a matching

   \vfill

   \begin{tikzpicture}[thick]

      \pic{alldiff};

      \draw[arc,take=<2>] (x3) -- (v1);

      \draw[arc,take=<2>] (x4) -- (v2);

      \draw[arc,take=<2>] (x5) -- (v3);

      \draw[arc,take=<3>] (x2) -- (v2);

      \draw[arc,take=<3>] (x3) -- (v3);

      \draw[arc,take=<3>] (x4) -- (v4);

      \draw[arc,take=<3>] (x5) -- (v5);

      \node[matrixnode,good=<3->] at (x2) {$x_2$};

      \node[matrixnode,good=<2->] at (x3) {$x_3$};

      \node[matrixnode,good=<2->] at (x4) {$x_4$};

      \node[matrixnode,good=<2->] at (x5) {$x_5$};

   \end{tikzpicture}

\end{frame}

\begin{frame}{Improving a matching}

   \begin{enumerate}

      \item find a free vertex $x$

         \vfill

      \item if $\exists (x, v)\in G$ s.\ t.\ $v$ is not matched, add it to $M$

         \vfill

      \item otherwise
         \begin{enumerate}

            \item take $(x, v)$ such that $(y, v)\in M$

            \item restart at 1 but using $y$ instead of $x$
         \end{enumerate}

   \end{enumerate}

\end{frame}

\begin{frame}{Improving a matching}

   \begin{tikzpicture}[thick]

      \pic{alldiff};

      \draw[arc,take=<1->] (x3) -- (v1);

      \draw[arc,take=<1-2>] (x4) -- (v2);

      \draw[arc,take=<1->] (x5) -- (v3);

      \node[matrixnode,good=<2>] at (x2) {$x_2$};

      \draw[arc,alt=<2>{inriared}{alt=<3->{inriableu}{}}] (x2) -- (v2);

      \draw[arc,bad=<2>] (x2) -- (v3);

      \node[matrixnode,good=<3>] at (x4) {$x_4$};

      \node[matrixnode,good=<4>] at (v4) {$4$};

      \draw[arc,take=<5>] (x4) -- (v4);

   \end{tikzpicture}

\end{frame}

\begin{frame}{Does it always work?}

   \pause%
   No! It can \pause%
   loop\dots

   \pause\vfill

   It works if we can find an \emph{odd-length} \textbf{alternating path} (one
   edge in $M$ one not in $M$), starting and ending in a \emph{free vertex}

   \vfill

   Enforce alternation by orienting $G$ as follows:
   \begin{itemize}

      \item $(x, v)\in M$, orient it as $v\rightarrow x$

      \item $(x, v)\not\in M$, orient it as $x\rightarrow v$

   \end{itemize}

\end{frame}


\begin{frame}{Alternating path}

   \begin{tikzpicture}[thick]

      \pic{alldiff};

      \draw[->,arc] (x1) -- (v1);
      \draw[->,arc] (x1) -- (v2);

      \draw[->,arc] (x2) -- (v3);

      \draw[->,arc] (x3) -- (v3);

      \draw[->,arc] (x5) -- (v4);
      \draw[->,arc] (x5) -- (v5);
      \draw[->,arc] (x5) -- (v6);

      \draw[->,arc] (x6) -- (v6);
      \draw[->,arc] (x6) -- (v7);

      \draw[<-,arc,take=<1->] (x3) -- (v1);

      \draw[<-,arc,goodarc=<4->,take=<1-3>]
         (x4) -- (v2);

      \draw[<-,arc,take=<1->] (x5) -- (v3);

      \node[matrixnode,good=<2->] at (x2) {$x_2$};

      \draw[->,arc,goodarc=<3->] (x2) -- (v2);

      \node[matrixnode,good=<5->] at (v4) {$4$};

      \draw[->,arc,goodarc=<5->] (x4) -- (v4);

   \end{tikzpicture}

   \visible<6->{How do we find such a path?}

   \visible<7->{DFS (or any other similar algorithm), $\cal{O}(\lvert V\rvert
   + \lvert E\rvert)$}

   \vfill

   \visible<8->{Update arrows and iterate}

   \vfill

   \visible<9->{No path starting from free \emph{variable} $x_i$
   means that $x_i$ not in the maximum matching}

\end{frame}

\begin{frame}{Summary}

   To check for satisfiability of the \texttt{alldifferent} constraint

   \pause\vfill

   build a graph $G$ with $V = \{x_i\}\cup\{v_i\}$ and $E = \{(x, v)\mid v\in
   D(x)\}$

   \pause\vfill

   look for a \textbf{maximum matching} by iterative improvement using DFS for
   alternating paths in a directed version of $G$

   \pause\vfill

   if $\lvert M\rvert = \lvert X\rvert$ we have satisfiability

   \vfill

   and now?

\end{frame}

\pictureframe{ClaudeBerge.jpg}{\Large\pause\raggedleft%
Claude\\
Berge\\
1926--2002}

\begin{frame}{Claude Berge}

   One of the co-founders of French literary group \textbf{Oulipo}

   \vfill

   Great-grandson of French President Félix Faure

   \vfill

   Two conjectures in the 60s on \emph{perfect graphs} proven much later

   \vfill

   Notions of acyclicity for \textbf{hypergraphs} (e.g.\ constraint
   hypergraphs!)

   \vfill

   \textbf{Berge's lemma} about maximum matchings

\end{frame}

\begin{frame}{Berge's lemma}

   An edge is considered \emph{free} if it belongs to a maximum matching but does
   not belong to all maximum matchings.

   \vfill

   An edge $e$ is \emph{free} if and only if, in an arbitrary maximum matching
   $M$, the edge $e$ belongs to an \textbf{even alternating path starting at an
   unmatched vertex} or to an \textbf{alternating cycle}.

\end{frame}

\begin{frame}{How do we use that?}

   Find a maximum matching $M$

   \pause\vfill

   Start from a free \textbf{value}

   \pause\vfill

   look for a path $\pi$ in $G$ with the \textbf{opposite orientation} as
   before

   \pause\vfill

   any arc in $\pi$ belongs to some maximum matching \pause(i.e., solution)

   \pause\vfill

   SCCs are alternating cycles, arcs also belong to some solution

   \pause\vfill

   filter edges \pause%
   \textbf{not in $M$, nor in $\pi$ nor in an SCC}

\end{frame}

\begin{frame}{Filtering}

   \vspace*{-8mm}
   \begin{align*}
      D(x_1)&=\{1,2\} & D(x_2)&=\{2,3\} & D(x_3)&=\{1,3\}\\
      D(x_4)&=\{2,4\} & D(x_5)&=\{3,4,5,6\} & D(x_6)&=\{6,7\}
   \end{align*}
   \begin{tikzpicture}[thick]

      \pic{alldiff};

      \draw[<-,arc,goodarc=<7->] (x1) -- (v1);
      \draw[->,arc,take=<1->,goodarc=<8->] (x1) -- (v2);

      \draw[->,arc,take=<1->,goodarc=<10->] (x2) -- (v3);
      \draw[<-,arc,goodarc=<9->] (x2) -- (v2);

      \draw[->,arc,take=<1->,goodarc=<12->] (x3) -- (v1);
      \draw[<-,arc,goodarc=<11->] (x3) -- (v3);

      \draw[->,arc,take=<1->] (x4) -- (v4);
      \draw[<-,arc,bad=<13->] (x4) -- (v2);

      \draw[<-,arc,bad=<13->] (x5) -- (v3);
      \draw[<-,arc,bad=<13->] (x5) -- (v4);
      \draw[->,arc,take=<1-5>,goodarc=<6->] (x5) -- (v5);
      \draw[<-,arc,goodarc=<5->] (x5) -- (v6);

      \draw[->,arc,take=<1-3>,goodarc=<4->] (x6) -- (v6);
      \draw[<-,arc,goodarc=<3->] (x6) -- (v7);

      \node[matrixnode,good=<2->] at (v7) {$7$};

   \end{tikzpicture}
   \vspace*{-3mm}
   \visible<14->{
      \begin{align*}
         D(x_1)&=\{1,2\} & D(x_2)&=\{2,3\} & D(x_3)&=\{1,3\}\\
         D(x_4)&=\{4\} & D(x_5)&=\{5,6\} & D(x_6)&=\{6,7\}
      \end{align*}
   }

\end{frame}

\section*{Catalog}

\begin{frame}{423 global constraints}

   Global Constraint Catalog (\url{http://sofdem.github.io/gccat/})

   \vfill

   Created by Nicolas Beldiceanu (EMN) in 2006

   \vfill

   aims at an exhaustive characterization of all global constraints, with
   their filtering algorithm(s)  %  chktex 36

   \vfill

   as comparison, only about 100 global constraints in \texttt{globals.mzn}

\end{frame}

\begin{frame}{Common global constraints}

   \texttt{alldifferent}, N-queens, Sudoku, any mutual exclusion, \dots

   \vfill

   \texttt{minimum}/\texttt{maximum}, imposes that the value of one variable is
   the min/max of those of other variables.

   Bidirectional!

   \vfill

   \texttt{global\_cardinality}, specifies the number of occurrences of each
   value in a list of variables

   Can be used for magic-series, and derived in \texttt{atleast},
   \texttt{atmost}, etc.

   \textbf{Flow algorithm} for consistency

\end{frame}

\begin{frame}{Common global constraints}

   \texttt{lex\_chain}, imposes that vectors are lexicographically
   ordered

   \vfill

   Most common usage will be given next week

   \vfill

   Can be encoded with reified constraints

   \vfill

   but has a dedicated filtering algorithm based on computing tight
   lower/upper bounds

\end{frame}

\begin{frame}{Common global constraints}

   \texttt{cumulative}, limits the
   \emph{capacity} of a machine handling several tasks $(s_i, d_i)$ at any
   point in time

   \vspace*{-3mm}
   \begin{center}
      \includegraphics[height=.4\textheight]{cumulative}
   \end{center}

   \vspace*{-3mm}
   Filtering based on computing \emph{compulsory parts}, but if all durations
   (and heights) are fixed $\Rightarrow$ \textbf{balancing knapsack}
   constraint (i.e., dynamic programming)

\end{frame}
\section*{Decomposition}

\begin{frame}{Semantic decomposition}

   \textbf{Same solutions}, but simpler (binary, sometimes ternary)
   constraints


   Typical example \texttt{alldifferent}

   \vfill

   One can allow extra variables and project solutions


   e.g., \texttt{exactly}$([x_1,\dots,x_n], k, v)$ can be decomposed using
   $n+1$ extra variables $b_0,\dots,b_n\in\{0,\dots,n\}$, such that:\pause%

   \begin{itemize}

      \item $b_0 = 0$

      \item $(x_i = v\wedge b_i = b_{i-1}+1)\vee(x_i \not=v\wedge b_i=b_{i-1})$

      \item $b_n = k$

   \end{itemize}

\end{frame}

\begin{frame}{AC-decomposition}

   Semantic decomposition is actually always feasible, and doesn't help with
   filtering\dots

   \vfill

   We want the same solutions but also the same \textbf{level of propagation}

   \vfill

   Not the case for \pause\texttt{alldifferent}

   but true for \pause\texttt{exactly}

   \vfill

   Why? \pause$\Rightarrow$ we need to call Claude Berge to the rescue again!

\end{frame}

\begin{frame}{Hypergraphs and constraints}

   An \emph{hypergraph} $H = (V, E)$ is a generalization of a graph where the
   edges $E$ are arbitrary subsets of the vertices $V$

   \vfill

   The \emph{incidence graph} of $H$ is the bipartite graph with vertices
   $V\uplus E$ and edges:
   $\{(v, e)\mid v\in e \text{ in } H\}$

   \begin{center}
      \begin{tikzpicture}[thick]

         \node[draw] (A) at (1, 1) {A};
         \node[draw] (B) at (2, 1) {B};
         \node[draw] (C) at (0, 0) {C};
         \node[draw] (D) at (1, 0) {D};

         \node[draw,very thick,visible on=<1>,
            inriared,rounded corners,fit=(A) (B) (D)] {};
         \node[draw,very thick,visible on=<1>,
            inriableu,rounded corners,fit=(C) (D)] {};

         \node[draw,inriared,visible on=<2>] (ABD) at (2, 0) {};
         \node[draw,inriableu,visible on=<2>] (CD) at (0, 1) {};

         \draw[visible on=<2>] (ABD) -- (A);
         \draw[visible on=<2>] (ABD) -- (B);
         \draw[visible on=<2>] (ABD) -- (D);
         \draw[visible on=<2>] (CD) -- (C);
         \draw[visible on=<2>] (CD) -- (D);
      \end{tikzpicture}

   \end{center}

   an n-ary constraint can be seen as an hyperedge in the hypergraph of
   constraints

\end{frame}

\begin{frame}{Berge acyclicity}

   An hypergraph is \textbf{Berge-acyclic} iff its \emph{incidence graph} is
   acyclic

   \vfill

   Very strict:
   no hyperedge should intersect any other hyperedge with cardinal $>1$

   \vfill

   \textbf{Theorem:} If the decomposition of a constraint is Berge-acyclic,
   AC on the decomposition is equivalent to AC on the original constraint

   \pause\vfill

   Sketch of proof:
   the choice of values for the satisfiability of the decomposed constraints
   is \emph{compatible}

\end{frame}

\begin{frame}{Even better\dots}

   Actually, if the decomposition is \textbf{Berge-acyclic} it is enough to
   propagate twice each of the decomposed constraints

   \pause\vfill

   The constraint hypergraph is a tree/forest (acyclic):
   propagate from leaves and contract, until you reach a single constraint,
   then propagate in the opposite order

\end{frame}

\begin{frame}{Example: \texttt{exactly}$(x, v, k)$}

   $(x_i = v\wedge b_i = b_{i-1}+1)\vee(x_i \not=v\wedge b_i=b_{i-1})$

   \vfill

\begin{tikzpicture}[thick]

      \matrix [
         matrix of math nodes,
         row sep=1cm,
         column sep=3mm,
         nodes={
            draw, minimum size=7mm, text width=8mm,
            inner sep=0pt, align=center, font=\small,
            text height=1.5ex, text depth=.25ex},
      ] {
         \& |(x1)| x_1 \& \& |(x2)| x_2 \& \& |(x3)| x_3 \& \&
         |[draw=none]| \cdots\& \\
         |(b0)| b_0 \& \& |(b1)| b_1 \& \& |(b2)| b_2 \& \& |(b3)| b_3\&
            \& |[draw=none]| \cdots\\
      };

      \node[draw,very thick,
         inriableu,rounded corners,fit=(x1) (b0) (b1)] {};
      \node[draw,very thick,
         inriavert,rounded corners,fit=(x2) (b1) (b2)] {};
      \node[draw,very thick,
         inriared,rounded corners,fit=(x3) (b2) (b3)] {};

   \end{tikzpicture}

   \pause\vfill

   Propagate from one end or the other

   \pause\vfill

   What if we had decomposed \texttt{exactly} using \textbf{reified
   constraints}?\pause\hfil
   $\bigwedge(b_i\Leftrightarrow x_i = v)$
   \pause$\displaystyle\wedge\sum_{b_i}=k$\pause\vfill

   Still \emph{Berge-acyclic} \pause(star shape) \pause%
   but \texttt{sum}!

\end{frame}

\begin{frame}{``With great power comes great responsibility''}

   \pause(French National Convention, May 8th 1793 « une grande responsabilité
   est la suite inséparable d’un grand pouvoir »)

   \vfill

   Global constraints are $n$-ary constraints, with \textbf{dedicated
   algorithms for efficient propagation}

   \vfill

   they can sometimes be decomposed, but the cost might not be negligible
   (loss of filtering, additional variables, \dots)

   \vfill

   When they are available, they are very powerful, so, use them!

\end{frame}

\againframe{docker}
\end{document}
