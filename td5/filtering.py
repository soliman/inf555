"""Let us encode the N-queens problem with a simple FD constraint solver."""
import string

from constraint import Constraint, Model, State, Var, X_different_from_C, X_equal_C


### Write the explanation of what you see after the first call to
### propagate_once_each() here

### Which Constraint Propagation rule does the call to is_entailed implement?

### Write the explanation of what you see after we add a constraint here

### Write the explanation of what you see after the last call to
### propagate_once_each() here


def n_queens_model(number: int) -> Model:
    """N variables using the letters of the alphabet.

    (only works up to 26, obviously)
    """
    variables = list(string.ascii_uppercase[:number])
    # we add the variables and put the initial domains 1..N in a single step
    return Model(1, number, variables)


def add_n_queens_constraints(model: Model):
    """Add constraints for the usual N-queens problem."""
    variables = model.state.keys()
    for i, x in enumerate(variables):
        for j, y in enumerate(variables):
            if i < j:
                model.add_constraint(X_different_from_Y_plus_C(x, y, 0))
                model.add_constraint(X_different_from_Y_plus_C(x, y, j - i))
                model.add_constraint(X_different_from_Y_plus_C(x, y, i - j))


class X_different_from_Y_plus_C(Constraint):
    """Simple propagator for the constraint x != y + c.

    it only propagates something when one of the variables is instantiated
    """

    def __init__(self, x: Var, y: Var, c: int):
        self.x = x
        self.y = y
        self.c = c

    def __str__(self):
        return f'{self.x} != {self.y} + {self.c}'

    def apply(self, state: State) -> bool:
        has_changed = False
        if state.is_instantiated(self.x):
            val = state.get_value(self.x) - self.c
            has_changed |= val in state[self.y]
            state[self.y].remove_value(val)
        if state.is_instantiated(self.y):
            val = state.get_value(self.y) + self.c
            has_changed |= val in state[self.x]
            state[self.x].remove_value(val)
        return has_changed


class X_greaterthan_Y_plus_C(Constraint):
    # Write your answer here
    pass


def repeated_inconsistent_propagation():
    # Write your answer here
    pass


class X_equal_Y_plus_C(Constraint):
    # Write your answer here
    pass


def difference_between_equality_and_double_gt():
    # Write your answer here
    pass


class X_times_Y_equal_Z(Constraint):
    # Write your answer here
    pass
