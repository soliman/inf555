{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TD7: Air Traffic Control\n",
    "\n",
    "This practical session is about a real-world problem in air traffic control, treated in 1991 in Prolog with constraints when your teacher was at CNRS and consultant at Thomson-CSF/Thales.\n",
    "\n",
    "Today you are asked to use MiniZinc with finite domain constraints to model and solve this optimization problem.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Traffic regulation on arrivals\n",
    "Air traffic control in the terminal area corridors of an airport includes all arriving flights within 40 minutes of the runway. Scheduling the landing of flights is a critical task during peak periods. At an airport such as Orly, the arrivals can be regulated by delaying the departures of domestic flights to Orly, however, departure regulations do not apply to long-haul flights. The purpose of this TD7 is to show, using constraint-based modeling and optimization tools, that unnecessary delays can be avoided by optimizing the sequence of landing flights.\n",
    "\n",
    "* Each flight has an **initially expected time of arrival**, counted in seconds from the current time 0. \n",
    "* The **actual time of arrival** that we want to compute must be located within a **strict range** (i.e. bounds excluded) determined by a delay factor, set by default to 3 times the expected time of arrival, and an advance factor, set to 0.8 by default, with respect to the initially expected time of arrival.\n",
    "\n",
    "The terminal area of an airport is divided in space with several runway approach **corridors**.\n",
    "* Each flight is in a corridor and can not leave it. \n",
    "\n",
    "The terminal area is also divided in time in three areas:\n",
    "* the **critical area** (5 min from the landing track, 5mn included) where no permutation of flights is possible,\n",
    "* the **regulated area** (5 to 20 minutes from the track, 20 mn included) where permutations between flights in different corridors only are possible,\n",
    "* the **third zone** (20 to 40 min from the track) where all permutations of flights are possible even within the same air corridor.\n",
    "\n",
    "Changes to flights are made by giving commands to the pilot who can modify the speed of the aircraft or the trajectory (by widening  a curve or making loops), but we will not directly deal with these aspects here.\n",
    "\n",
    "The distances to be respected between flights depend on the size of the aircrafts.\n",
    "* There are three categories of aircrafts: A (large), B (medium), C (small).\n",
    "* Because of the air turbulences created, small aircrafts behind a large aircraft need a larger distance than the opposite.\n",
    "* The **matrix of safety distances**, Dij, expresses in seconds the minimum **strict time separation** between i and j if i precedes j, as follows:\n",
    "\n",
    "| Dij | A |  B  | C |\n",
    "| --- | --- | --- | --- |\n",
    "| A | 100 | 130 | 160 |\n",
    "| B | 70 | 80 |100|\n",
    "| C | 60 | 60 | 70 |\n",
    " \n",
    "For the sake of simplification, it will be sufficient to apply these **time separation constraints at landing time**, that is to say at the computed time of arrival, **not in all intermediate points**.\n",
    "\n",
    "The strategy of the air traffic controllers essentially consists in preserving the order of the sequence of arrivals (First-In First-Out FIFO strategy) by advancing and delaying the flights in order to satisfy the time separation constraints between aircrafts. However, since the distance matrix is not symmetrical, it is possible to minimize the overall time of arrival of the set of flights by permuting some flights in the expected sequence of arrivals. The search for a solution of minimum cost then becomes an NP-difficult problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data from Orly Airport\n",
    "\n",
    "The vintage image below shows a screen copy of a system developed in 1991 in Prolog with constraints for finding optimal solutions. \n",
    "\n",
    "The track is depicted on the left, the three corridors are materialized by the horizontal bands. \n",
    "\n",
    "In the lower part of the screen are the initially expected time of arrival as well as the solution resulting from the First-In First-Out FIFO strategy preserving the order of arrival (28.67 mn in this example). \n",
    "\n",
    "The upper part shows an optimal solution of proven optimal cost 26.5 mn. This solution consists in permuting flights 6 and 7-8, 12 and 13, 14 and 15. \n",
    "\n",
    "<img src=\"ATC.png\">\n",
    "\n",
    "The file **ATCorly.dzn contains a data set** for\n",
    "* 15 flights\n",
    "* in the 3 air corridors of Orly.\n",
    "In this example, \n",
    "the minimum time for landing the sequence of flights keeping the order of the sequence (FIFO strategy) is 23.58 minutes (i.e. 23mn35s)\n",
    "\n",
    "The **ATCsydney.dzn** is another dataset of\n",
    "* 20 flights\n",
    "* in 3 air corridors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import inf555"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[0m\u001b[K\u001b[0;34m%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% Data file from Orly airport\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% The flights are assumed to be sorted by increasing order of expected arrival time\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% number of flights\r\n",
      "\u001b[0m\u001b[K\u001b[0mnf = \u001b[0;31m15\u001b[0m; \r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% number of corridors\r\n",
      "\u001b[0m\u001b[K\u001b[0mnc = \u001b[0;31m3\u001b[0m;\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% expected time of arrival in seconds\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0mexpectedtime = [\u001b[0;31m75\u001b[0m,\u001b[0;31m180\u001b[0m,\u001b[0;31m265\u001b[0m,\u001b[0;31m270\u001b[0m,\u001b[0;31m285\u001b[0m,\u001b[0;31m320\u001b[0m,\u001b[0;31m330\u001b[0m,\u001b[0;31m435\u001b[0m,\u001b[0;31m565\u001b[0m,\u001b[0;31m660\u001b[0m,\u001b[0;31m685\u001b[0m,\u001b[0;31m715\u001b[0m,\u001b[0;31m735\u001b[0m,\u001b[0;31m855\u001b[0m,\u001b[0;31m880\u001b[0m];\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% aircraft categories (A=1, B=2, C=3)\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0mcategory = [\u001b[0;31m2\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m];\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% aircraft corridors\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0mcorridor = [\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m];\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0m\u001b[0m\r\n"
     ]
    }
   ],
   "source": [
    "!vimcat.sh ATCorly.dzn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[0m\u001b[K\u001b[0;34m%%%%%%%%%%%%%%%%%\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% Larger data set\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m%%%%%%%%%%%%%%%%%\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% The flights are assumed to be sorted by increasing order of expected arrival time\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% number of flights\r\n",
      "\u001b[0m\u001b[K\u001b[0mnf = \u001b[0;31m20\u001b[0m; \r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% number of corridors\r\n",
      "\u001b[0m\u001b[K\u001b[0mnc = \u001b[0;31m3\u001b[0m;\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% expected time of arrival in seconds\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0mexpectedtime = [\u001b[0;31m75\u001b[0m, \u001b[0;31m180\u001b[0m, \u001b[0;31m265\u001b[0m, \u001b[0;31m270\u001b[0m, \u001b[0;31m285\u001b[0m, \u001b[0;31m320\u001b[0m, \u001b[0;31m330\u001b[0m, \u001b[0;31m435\u001b[0m, \u001b[0;31m565\u001b[0m, \u001b[0;31m660\u001b[0m, \u001b[0;31m685\u001b[0m, \u001b[0;31m690\u001b[0m, \u001b[0;31m715\u001b[0m, \u001b[0;1;37;41m720, 735, 740, 855, 860, 880, 885];\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% aircraft categories (A=1, B=2, C=3)\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0mcategory = [\u001b[0;31m2\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m3\u001b[0m];\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0;34m% aircraft corridors\r\n",
      "\u001b[0m\u001b[K\u001b[0m\r\n",
      "\u001b[0m\u001b[K\u001b[0mcorridor = [\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m3\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m2\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m,\u001b[0;31m1\u001b[0m];\u001b[0m\r\n"
     ]
    }
   ],
   "source": [
    "!vimcat.sh ATCsydney.dzn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 1. MiniZinc types and variables\n",
    "### Define in a file ATC.mzn \n",
    "* the types and variables for the data of the problem\n",
    "* the distance matrix\n",
    "* the decision variables, i.e. the array of computed times of arrival,\n",
    "* the predicates advance(i) and delay(i) that post the time range constraints with respect to the expected time of arrival of flight i\n",
    "* the range constraints for all flights\n",
    "* the predicate precedes(i, j) that posts the distance constraint if flight i precedes flight j "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!vimcat.sh ATC.mzn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 2. First-In First-Out Strategy in MiniZinc\n",
    "### Define in a file ATCfifo.mzn\n",
    "* the inclusion of file ATC.mzn\n",
    "* the distance constraints for the FIFO strategy\n",
    "* the search using the FIFO strategy as heuristics\n",
    "* the output for printing the global landing time, the solution found, and the computation time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!vimcat.sh ATCfifo.mzn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Execute the FIFO strategy on Orly and Sydney data\n",
    "The global landing time should be 23.58mn (i.e. 23mn35s) on Orly data and 30.17mn (i.e. 30mn10s) on Sydney data "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCfifo.mzn', 'ATCorly.dzn'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCfifo.mzn', 'ATCsydney.dzn'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 3. Time Window Constraint Satisfiability \n",
    "Before dealing with the optimization problem, let us first deal with the problem of checking the satisfiability of the constraints of the  problem for a given sequence of fligths.\n",
    "\n",
    "We recommend to use reified constraints to express disjunctive precedence constraints as in the bridge.mzn \n",
    "\n",
    "### Define in file ATCsat.mzn\n",
    "* the critical area constraints (avoiding to post the transition closure of the precedence constraints)\n",
    "* the regulated area constraints\n",
    "* the third area constraints\n",
    "* the search for satisfiability "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!vimcat.sh ATCsat.mzn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCsat.mzn', 'ATCorly.dzn'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCsat.mzn', 'ATCsydney.dzn'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you used the same and-choice and or-choice heuristics as for the FIFO strategy you should have computed the same solution, but here with the general constraints of the problem.\n",
    "\n",
    "# Question 4. Computing an Optimal ATC Solution\n",
    "\n",
    "\n",
    "### Define in a file ATCoptim.mzn\n",
    "* the objective function and the search to mimize the global landing time of the flights\n",
    "\n",
    "What is the optimal cost ?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!vimcat.sh ATCoptim.mzn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCoptim.mzn', 'ATCorly.dzn'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCoptim.mzn', 'ATCsydney.dzn'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the optimal cost ?\n",
    "\n",
    "How far was the FIFO strategy from the optimal solution ?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Question 5. Limited Discrepancy Search\n",
    "We could also assume that the **number of flight permutations is bounded** in the global sequence.\n",
    "\n",
    "This further constraint can be implemented by **Limited Discrepancy Search (LDS)** with the **or-choice heuristics of not permuting flights**. Indeed, LDS then consists in limiting the traversal of the search tree to the branches with a maximum number of flight permutations. \n",
    "\n",
    "### Define in a file ATClds.mzn\n",
    "* implement LDS with reified constraints\n",
    "* apply LDS for computing (sub)optimal solutions with at most 3 flight permutations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!vimcat.sh ATClds.mzn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATClds.mzn', 'ATCorly.dzn', data={'max_number_permutations':3}))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATClds.mzn', 'ATCsydney.dzn', data={'max_number_permutations':3}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bonus Question: Greedy Heuristics\n",
    "* Examine the swaps done in the optimal solutions compared to the FIFO strategy\n",
    "* Find a heuristic criterion for making a good swap\n",
    "* Invent a (deterministic) greedy heuristic strategy better than FIFO (if not, look again! )\n",
    "\n",
    "### Implement your new deterministic strategy in file ATCgreedy.mzn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!vimcat.sh ATCgreedy.mzn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCgreedy.mzn', 'ATCorly.dzn'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(inf555.minizinc('ATCgreedy.mzn', 'ATCsydney.dzn'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluate the three algorithms by comparing the costs of the solutions and the computation times\n",
    "How far is your strategy from the optimal and FIFO solutions ?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
