%-*- TeX-engine: xetex; -*-
\documentclass[tikz,code,blocks,aspectratio=169]{lifewareslides}

\title{Boolean satisfiability, SAT solvers (part 1)}

\author{Sylvain Soliman}

\date{September 30th, 2020\\
\bigskip{\scriptsize Thanks to Lintao Zhang and David L.\ Dill for inspiration}}

\begin{document}

\begin{frame}{Docker setup}

   You can start

   {\small\lstinline|docker pull registry.gitlab.inria.fr/soliman/inf555/td2|}

   now

\end{frame}

\begin{frame}
   \titlepage%
\end{frame}

\section*{Introduction}

\begin{frame}{Propositional Logic}

   Propositional formulae are built from the following:

   \vfill

   \begin{itemize}

      \item Constants: $1$ and $0$

      \item Propositional variables: $a, b, p, q, x, y, \dots$

      \item Logical Connectives: $\vee,\wedge,\neg$

         (others too, e.g., $(a\Rightarrow b)\Leftrightarrow(\neg a\vee b)$)

   \end{itemize}

   \vfill

   No quantifiers, no functions

   \vfill

   A formula itself is a function $\mathbb{B}^n \mapsto \mathbb{B}$ of its $n$
   variables

   \vfill

   \textbf{Satisfiable} \emph{iff} $\exists$ mapping of variables to constants
   such that the formula becomes true (tautology)

\end{frame}

\begin{frame}{Boolean Satisfiability problem}

   \begin{defi}

      The Boolean satisfiability problem (SAT) is the decision problem whether
      a propositional formula $\phi$ is satisfiable, i.e., $\phi^{-1}(1)
      \not\equal\emptyset$

   \end{defi}

   \vfill

   First \textbf{NP-complete} problem (Cook 1971)

   \vfill

   \textcolor{inriared}{$\blacktriangleright$} solving SAT efficiently
   provides a way to solve any other NP problem rather efficiently

\end{frame}

\begin{frame}{Combinatorial Problems}

   Most combinatorial problems (AI planning, theorem proving, model
   checking, resource allocation, etc.) are in NP

   \vfill

   Ubiquitous in the industry

   \vfill

   Solver competitions in the academia

   \vfill

   \textcolor{inriared}{$\blacktriangleright$} SAT was well studied for the
   last 40 years and now very efficient (millions of variables)

   \vfill

   \textbf{We will see how SAT-solvers work and how to use one}

\end{frame}

\section*{Conjunctive Normal Form}

\begin{frame}{Formula Representation}

   Representing formulae by the state space, i.e., the Boolean hypercube
   $\mathbb{B}^n$ is exponential

   \vfill

   Same for truth tables

   \vfill
   \vfill

   Better reprentations:

   \begin{itemize}

      \item Boolean circuit

      \item Binary Decision Diagram (BDD)

      \item Boolean formula in \textbf{normal form}

   \end{itemize}

\end{frame}

\begin{frame}{Conjunctive Normal Form --- CNF}

   Used in most modern SAT solvers

   \vfill
   \vfill

   Functions represented as a big conjunction ($\wedge$ sometimes noted
   multiplicatively)

   \vfill

   of \textbf{clauses}, i.e., disjunctions ($\vee$ sometimes noted $+$)

   \vfill

   of \textbf{literals} (a variable $x$ or its negation $\neg x$ or
   $x'$)

   \vfill
   \vfill

   $(x'(y'+z'))' = x + yz$

\end{frame}

\begin{frame}{Normalization}

   Eliminate constants:
   \begin{itemize}

      \item $x1 = x$

      \item $x0 = 0$
   \end{itemize}

   Optionally do more simplifications
   \begin{itemize}

      \item $xx=x$

      \item $xx'=0$

      \item etc.

   \end{itemize}

   If the formula is a constant, no need for CNF

   \vfill

   \emph{Careful!}

   applying distribution leads to an exponential blowup

\end{frame}

\begin{frame}{Tseytin's Transformation}

   Add \textbf{new variables} for subformulae of the original
   formula, e.g.

   \[\phi = pq + \psi\]

   \vfill

   Introduce $x\Leftrightarrow p\wedge q$, i.e., $(x\Rightarrow (p\wedge
   q))\wedge((p\wedge q)\Rightarrow x)$

   \vfill

   $(x' + pq)((pq)' + x)$

   \vfill

   $(x' + p)(x' + q)(p' + q' + x)$

   \vfill

   \[\phi = (x + \psi)(x'+p)(x'+q)(p'+q'+x)\]

\end{frame}

\begin{frame}{Example --- The N-Queens revisited}

   Given an integer $N$, write a (mathematical) Boolean satisfiability problem
   representing the N-Queens problem that you already saw last week

   \vfill

   All variables need to be Boolean

   \vfill

   Try to write the constraints in CNF

\end{frame}

\begin{frame}{Clauses Optimizations}

   Once in clause form:

   \vfill

   $(p+p+\dots) = (p+\dots)$

   \vfill

   $(p+p'+\dots)(\psi) = \psi$

   \vfill

   Avoid introducing two variables in Tseytin's transformation for $\phi$ and
   $\neg\phi$

   \vfill

   Now, we have to solve:

   $(\textcolor<2>{inriableu}{p}+\textcolor<2>{inriableu}{q}+
   \textcolor<2>{inriableu}{r})(\textcolor<2>{inriared}{p'}+
   \textcolor<2>{inriared}{q'}+
   \textcolor<2>{inriableu}{r})(\textcolor<2>{inriared}{p'}+
   \textcolor<2>{inriableu}{q}+
   \textcolor<2>{inriared}{r'})(\textcolor<2>{inriableu}{p}+
   \textcolor<2>{inriared}{q'}+\textcolor<2>{inriared}{r'})$

\end{frame}

\section*{SAT solving}

\begin{frame}{Naïve Solver}

   A naïve solver could try possible assignments by backtracking

   \vfill

   Until either all clauses have a true literal (return SAT)

   \vfill

   or some clause has all false literals (return UNSAT)

   \vfill

   Very slow, unless early SAT or UNSAT result

\end{frame}

\begin{frame}

   \textrm{\small M.\ \textbf{\textcolor{inriared}{Davis}}, H.\
      \textbf{\textcolor{inriared}{Putnam}}, ``A computing procedure for
      quantification theory'', J.\ of ACM, Vol. 7, pp.\ 201--214, 1960}

   \vfill

   Iterative variable elimination and clause rewriting for clauses with
   \textbf{one} incompatible variable ($p$ and $p'$)

   \vfill

   $(a+\textcolor{inriared}{b}+c)(\textcolor{inriared}{b}+c'+f')
   (\textcolor{inriared}{b'}+e)\longrightarrow(a+\textcolor{inriableu}{c}+e)
   (\textcolor{inriableu}{c'}+e+f')\longrightarrow
   (a+e+f')\longrightarrow\text{SAT}$

   \vfill

   $(a+\textcolor{inriared}{b})(a+\textcolor{inriared}{b'})(a'+c)(a'+c')
   \longrightarrow(\textcolor{inriableu}{a})(\textcolor{inriableu}{a'}+c)
   (\textcolor{inriableu}{a'}+c')\longrightarrow(\textcolor{inriavert}{c})
   (\textcolor{inriavert}{c'})\longrightarrow\text{UNSAT}$

   \vfill

   If some literal is always positive (or negative), i.e.,
   \emph{\textbf{pure}}, don't try the other valuation

   \vfill

   Potential memory explosion issue

\end{frame}

\begin{frame}

   \textrm{\small M.\ \textbf{\textcolor{inriared}{Davis}}, G.\
      \textbf{\textcolor{inriared}{Logemann}}, and D.\
      \textbf{\textcolor{inriared}{Loveland}}, ``A machine program for
      theorem-proving''. Communications of the ACM, Vol.\ 5, No.\ 7,
      pp.\ 394--397, 1962}

   \vfill

   Avoids EXPSPACE issue of Davis-Putnam

   \vfill

   Replace DP with \textbf{Depth First Search (DFS) and BCP}

   to obtain DPLL, the current basis of all SAT solvers

   \vfill

   \textbf{Unit propagation} a.k.a.\ Boolean Constraint 
   Propagation (\textbf{BCP}):

   \begin{itemize}

      \item[\textcolor{inriared}{$\blacktriangleright$}]if only one literal of
         a clause doesn't currently evaluate to $0$
         \textcolor{inriableu}{$\Rightarrow$} set it to $1$

   \end{itemize}

\end{frame}

\begin{frame}{DPLL example}

   $\textcolor<2-8,13-14>{inriavert}{(\textcolor<10-12>{inriaorange}{a'}+
   \textcolor<11>{inriaorange}{b}+\textcolor<11>{inriared}{c})}
   \textcolor<6,10-14>{inriavert}{(\textcolor<2-5,7-8>{inriaorange}{a}+
   \textcolor<4>{inriaorange}{c}+\textcolor<4>{inriared}{d})}
   \textcolor<6,10-14>{inriavert}{(\textcolor<2-5,7-8>{inriaorange}{a}+
   \textcolor<4>{inriaorange}{c}+\textcolor<4>{inriared}{d'})}
   \textcolor<4,10-14>{inriavert}{(\textcolor<2-3,5-8>{inriaorange}{a}+
   \textcolor<6>{inriaorange}{c'}+\textcolor<6>{inriared}{d})}$

   $\textcolor<4,10-14>{inriavert}{(\textcolor<2-3,5-8>{inriaorange}{a}+
   \textcolor<6>{inriaorange}{c'}+\textcolor<6>{inriared}{d'})}
   \textcolor<3-6,11>{inriavert}{(\textcolor<13-14>{inriaorange}{b'}+
   \textcolor<14>{inriaorange}{c'}+d)}
   \textcolor<2-8,13-14>{inriavert}{(\textcolor<10-12>{inriaorange}{a'}+
   \textcolor<11>{inriaorange}{b}+\textcolor<11>{inriared}{c'})}
   \textcolor<2-8,11,14>{inriavert}{(\textcolor<10,12-13>{inriaorange}{a'}+
   \textcolor<13>{inriaorange}{b'}+c)}$

   \begin{center}
      \begin{tikzpicture}[
         ->,>=stealth,thick,level/.style={sibling distance=5cm/####1},
         level distance=1.5cm]

         \node {a}
         child [visible on=<2->]{%
            node {b}
            child [visible on=<3->]{%
               node {c}
               child [visible on=<4->]{%
                  node [visible on=<5->]{$\bot$}
                  edge from parent node[above] {0}
               }
               child [visible on=<6->]{%
                  node [visible on=<7->]{$\bot$}
                  edge from parent node[above] {1}
               }
               edge from parent node[left] {0}
               edge from parent node[visible on=<8->, right] {Pure}
            }
            edge from parent node[above] {0}
         }
         child [visible on=<10->]{%
            node {b}
            child [visible on=<11->]{%
               node [visible on=<12->]{$\bot$}
               edge from parent node[above] {0}
            }
            child [visible on=<13->]{%
               node {c}
               child [visible on=<14->]{%
                  node {SAT (d=1)}
                  edge from parent node[left] {1}
                  edge from parent node[right] {BCP}
               }
               edge from parent node[above] {1}
            }
            edge from parent node[above] {1}
         }
         ;

      \end{tikzpicture}
   \end{center}

\end{frame}

\begin{frame}{C'est bien, mais pas suffisant~!}

   \textrm{\small J.\ P.\ Marques-Silva and K.\ A.\ Sakallah, ``GRASP --- A New Search
   Algorithm for Satisfiability'', Proc.\ ICCAD 1996.}

   \vfill

   Learning from conflicts:

   $a\wedge b\wedge c\Rightarrow \text{conflict}$

   \vfill

   is equivalent to:

   $\text{no conflict}\Rightarrow\neg a\vee\neg b\vee\neg c$

   \vfill

   When a conflict occurs, add a new clause to the initial problem!

\end{frame}

\begin{frame}{DPLL example revisited}

   $\textcolor<2->{inriavert}{(a'+b+c)}
   (\textcolor<2->{inriaorange}{a}+
   \textcolor<4->{inriaorange}{c}+\textcolor<4->{inriared}{d})
   (\textcolor<2->{inriaorange}{a}+
   \textcolor<4->{inriaorange}{c}+\textcolor<4->{inriared}{d'})
   \textcolor<4->{inriavert}{(\textcolor<2-3>{inriaorange}{a}+
   c'+d)}$

   $\textcolor<4->{inriavert}{(\textcolor<2-3>{inriaorange}{a}+
   c'+d')}
   \textcolor<3->{inriavert}{(b'+c'+d)}
   \textcolor<2->{inriavert}{(a'+b+c')}
   \textcolor<2->{inriavert}{(a'+b'+c)}$

   \vfill

   \visible<2->{$a=0$}\visible<3->{, $b=0$}\visible<4->{, $c=0$}
   \vfill

   \visible<4->{Conflict: $a=0\wedge c=0$, i.e., $a'c'$}

   \vfill

   \visible<5->{We can add the new non-conflict clause: $(a'c')' = (a + c)$}
\end{frame}

\begin{frame}{Backjumping}

   We can use conflicts to backtrack better:
   \textbf{Conflict-driven backtracking}

   \vfill

   A conflict clause will lead to BCP when the second from last variable gets
   instantiated

   \vfill

   Sometimes you can jump even more (cf.\ Unique implication point)

   \vfill

   In our example, adding $(a + c)$ will instantiate $c$ to $1$ by BCP as soon
   as $a = 0$ is chosen:
   we can backtrack to that level and proceed from here\dots

\end{frame}

\begin{frame}{DPLL example revisited}

   $\textcolor<2->{inriavert}{(a'+b+c)}
   \textcolor<4->{inriavert}{(\textcolor<2-3>{inriaorange}{a}+
   c+d)}
   \textcolor<4->{inriavert}{(\textcolor<2-3>{inriaorange}{a}+
   c+d')}
   (\textcolor<2->{inriaorange}{a}+\textcolor<4->{inriaorange}{c'}+
   \textcolor<4->{inriared}{d})$

   $(\textcolor<2->{inriaorange}{a}+\textcolor<4->{inriaorange}{c'}+
   \textcolor<4->{inriared}{d'})
   (b'+\textcolor<4->{inriaorange}{c'}+d)
   \textcolor<2->{inriavert}{(a'+b+c')}
   \textcolor<2->{inriavert}{(a'+b'+c)}
   \textcolor<4->{inriavert}{(\textcolor<2-3>{inriaorange}{a}+
   \textcolor<3>{inriableu}{c})}$

   \vfill

   \visible<2->{$a=0$}\visible<3->{\ BCP}\visible<4->{: $c=1$}

   \vfill

   \visible<4->{We get the $d$ conflict without needing to go through the
   choices for $b$ or $c$, only \textbf{unit propagation}}

\end{frame}

\section*{More}

\begin{frame}{There's more\dots}

   Most of the computation time is taken by BCP\@:
   \textbf{optimize unit propagation}

   \vfill

   The procedure remains a search procedure:

   use \textbf{heuristics} to choose variables and assignments

\end{frame}

\begin{frame}{Variable/Value selection heuristics}

   There are many heuristics

   \vfill

   For values (try True or False first?)

   \vfill

   But mostly for variables, e.g.,

   \begin{itemize}

      \item RAND (random)

      \item MOM (Maximum Occurrences on clauses of Minimum Size)

      \item DLIS/DLCS (Dynamic Largest Individual/Combined Sum in unresolved
         clauses)

      \item etc.

   \end{itemize}

   But there will be a course dedicated to heuristics in CSPs in general

\end{frame}

\section*{BCP}

\begin{frame}{Optimizing BCP}

   BCP takes a lot of time because of bookkeeping:

   \begin{itemize}

      \item Find all unit clauses;

      \item Detect conflicts;

      \item Detect satisfied clauses;

      \item Undo all of this on backtracking!

   \end{itemize}

   \vfill

   Many algorithms (SATO, Chaff, etc.) optimize this process with
   \textbf{significant impact} on the solver performance

   \vfill

   You will see the cost of naive BCP in TD

\end{frame}

\begin{frame}{Literal Counting Scheme}

   Basic optimization

   \vfill

   Maintain a count of \emph{non-false} literals in each clause

   \vfill

   Unit clause when $k = 1$

   The remaining literal has to be found and instantiated to $1$

   \vfill

   Conflict when $k = 0$

\end{frame}

\begin{frame}{Chaff}

   Idea:
   a clause can become Unit when \pause%
   \emph{all but one} of its literals are $0$\pause%

   \vfill

   Just pick two literals to \textbf{watch} and ignore the rest

   Maintain this as an \emph{invariant}

   \vfill

   Handle clauses with a single literal as a special case

   \vfill

   Maintain the state and pending assignments as stacks

   \vfill

   Undoing assignments on backtrack maintains our invariant!

\end{frame}

\begin{frame}{Example}

   \visible<13-14,17-18>{$\Rightarrow$}   % chktex  8
   $(\textcolor<2-13>{inriableu}{b} + \textcolor<2-17>{inriableu}{c} + a +
   \textcolor<14->{inriableu}{d} + \textcolor<18->{inriableu}{e})$\\
   \visible<5-7,15-16>{$\Rightarrow$}   % chktex  8
   $\textcolor<16->{gray}{(\textcolor<2-6>{inriableu}{a} +
   \textcolor<2-15>{inriableu}{b} +
   \textcolor<7-15>{inriableu}{c'})}$
   \visible<6>{Replace $a=0$ by an unassigned literal}\\
   \visible<8-10>{$\Rightarrow$}   % chktex  8
   $\textcolor<10->{gray}{(\textcolor<2-9>{inriableu}{a} +
   \textcolor<2-9>{inriableu}{b'})}$
   \visible<9>{Unit! Record assignment}\\
   \visible<11-12>{$\Rightarrow$}   % chktex  8
   $\textcolor<12->{gray}{(\textcolor<2-11>{inriableu}{a'} +
   \textcolor<2-11>{inriableu}{d})}$
   \visible<12>{Satisfied}\\
   \visible<3-4>{$\Rightarrow$}   % chktex  8
   $\textcolor<5->{gray}{(a')}$
   \visible<3-4>{Special case of BCP}\\   % chktex  8

\vfill

\visible<2->{\textcolor{inriableu}{Watched literals}}

\vfill

\visible<4->{\textbf{Current assignment: }} \visible<4->{$a=0$}
\visible<13->{$b=0$} \visible<17->{$c=0$}\\
\visible<4->{\textbf{Pending: }}
\only<10-12>{$b=0$}\only<16>{$c=0$}   % chktex  8
\visible<19>{Nothing to do, done with BCP!}

\vfill
\visible<5->{Maintain watched literals!}

\end{frame}

\setbeamercolor{background canvas}{bg=inriared}
\begin{frame}
   \color{white}

   {\LARGE

      ``You don't have to think too hard when you talk to teachers.''

   \vfill
   }
   {\Large
      \hfill --- J.\ D.\ Salinger
   }

   \vfill

\end{frame}

\setbeamercolor{background canvas}{bg=white}
\begin{frame}[fragile=singleslide]{Docker setup}

   You can start

   {\small\lstinline|docker pull registry.gitlab.inria.fr/soliman/inf555/td2|}

   now

   \vfill

   and then

   {\small
     \begin{lstlisting}
       docker run -p 8888:8888 -p 8080:8080 \
       -v "$PWD":/home/jovyan/work \
       registry.gitlab.inria.fr/soliman/inf555/td2
     \end{lstlisting}
   }

\end{frame}

\end{document}
