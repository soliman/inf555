FROM jupyter/minimal-notebook
LABEL Author="Sylvain Soliman <Sylvain.Soliman@inria.fr>"
LABEL Maintainer="Sylvain Soliman <Sylvain.Soliman@inria.fr>"

USER root

# libgl1 is for minizinc
RUN apt-get -qy update && apt-get install -qy libgl1 vim-nox minisat graphviz && apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/*

RUN pip install minizinc lark-parser nest-asyncio matplotlib numpy graphviz flake8 pylint

# editors
WORKDIR /home/$NB_USER
RUN git clone https://github.com/m00nlight/minizinc-mode
RUN git clone https://github.com/vale1410/vim-minizinc
RUN mkdir .vim && cp -r vim-minizinc/ftdetect vim-minizinc/syntax .vim

RUN mkdir Vimcat
ADD Vimcat/vimcat.sh Vimcat

ADD .emacs inf555.py /home/$NB_USER/

ENV CODE_VERSION 3.4.0
ENV CODE_SERVER code-server-${CODE_VERSION}-linux-x86_64
RUN wget https://github.com/cdr/code-server/releases/download/${CODE_VERSION}/${CODE_SERVER}.tar.gz \
       && tar xzf ${CODE_SERVER}.tar.gz \
       && rm ${CODE_SERVER}.tar.gz \
       && chmod a+x ${CODE_SERVER}/code-server \
       && ${CODE_SERVER}/code-server --install-extension ms-python.python \
       && ${CODE_SERVER}/code-server --install-extension DekkerOne.minizinc

# install minizinc
ENV MINIZINC_VERSION 2.4.3
RUN wget https://github.com/MiniZinc/MiniZincIDE/releases/download/${MINIZINC_VERSION}/MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz \
       && tar xzf MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz \
       && rm -Rf MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz \
       && mv MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64 Minizinc

ENV PATH ${HOME}/Minizinc/bin:${HOME}/Vimcat:${PATH}
ENV PYTHONPATH ${HOME}

RUN chown -R $NB_USER:users /home/$NB_USER/

USER $NB_USER

CMD ${CODE_SERVER}/code-server --auth none --disable-telemetry --bind-addr 0.0.0.0:8080 /home/jovyan/ & jupyter notebook TD.ipynb --no-browser --NotebookApp.token=''
