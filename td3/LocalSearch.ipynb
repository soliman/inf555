{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Local Search\n",
    "\n",
    "Today we will implement a few **Constraint-Based Local-Search** algorithms and evaluate them on our favorite Constraint Solving Problem:\n",
    "the N-Queens!\n",
    "\n",
    "---\n",
    "\n",
    "We will first have a brief look at the code provided in the file [cbls.py](cbls.py) (***that you should not modify!***).\n",
    "\n",
    "The class `Board` encodes our CSP (line-wise, as a `Board.state: List[Int]`), with ways to initialize it, to display the result, to compute the current cost, to define all neighbors, and their corresponding costs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cbls\n",
    "b = cbls.Board(n=8, random=True)\n",
    "print(b)\n",
    "print(\"Cost:\", b.cost())\n",
    "print(b.show_all_costs())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b.plot_all_costs()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cost is defined as the *total* number of pairs of queens in conflict."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%psource cbls.Board.cost"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All local-search algorithms will inherit from class `LocalSearch`.\n",
    "\n",
    "It requires subclasses to implement the selection method `select` and then uses that to `run` once the algorithm from its current state, or even `evaluate` the average cost obtained from a certain number of iterations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%psource cbls.LocalSearch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An example of algorithm is also provided: `RandomWalk`.\n",
    "\n",
    "The only meaningful parts are the initialization (that sets up a counter to limit the number of steps, and the currently best board/cost) and the selection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%psource cbls.RandomWalk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cbls.RandomWalk.evaluate(iterations=500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "Now is the time for you to write some code! Everything should be written in a file [local.py](local.py), the current notebook and [cbls.py](cbls.py) should not change.\n",
    "\n",
    "## ***Write a `HillClimbing` class in [local.py](local.py) that selects at each point the lowest-cost next move and stops when the cost cannot be strictly improved.***\n",
    "\n",
    "Note that the `select` method returns a 4-tuple with:\n",
    "- the selected move (or `None`)\n",
    "- the corresponding cost\n",
    "- the corresponding state\n",
    "- a boolean for the stopping criterion\n",
    "\n",
    "and that its `moves` argument is a list of pairs (move, cost).\n",
    "\n",
    "Note also that in Python you can provide a `key` argument to the `sort`, `min` and `max` builtins, that defines a function to be applied before comparing two elements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import local\n",
    "local.HillClimbing.evaluate(iterations=500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## ***Add an optional argument `limit` with default value 0 to the constructor of the `HillClimbing` class. Allow `limit` moves to states that have a cost equal to the best one before stopping.***\n",
    "\n",
    "These are the *diagonal moves* discussed in class.\n",
    "\n",
    "The signature of the constructor becomes:\n",
    "\n",
    "```python\n",
    "def __init__(self, limit: int = 0, **kwargs):\n",
    "    \"\"\"…\"\"\"\n",
    "    super().__init__(**kwargs)\n",
    "    …\n",
    "```    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "local.HillClimbing.evaluate(iterations=500, limit=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## ***Write in the same file a `SimulatedAnnealing` class.***\n",
    "\n",
    "It should have a class/instance parameter `cooling_rate: float = 0.01`.\n",
    "\n",
    "The constructor should set the initial temperature to a value of the order of the maximum possible cost of a move.\n",
    "\n",
    "The code is fairly similar to `RandomWalk` since the selection amounts to:\n",
    "- choosing a random possible move\n",
    "- deciding wether to accept it (because it is better, or because the current temperature allows it)\n",
    "- applying the cooling rate to the current temperature\n",
    "- if no move was selected and the temperature is smaller than 0.001, stopping"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "local.SimulatedAnnealing.evaluate(iterations=50)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## ***OPTIONAL: play with the parameters (initial temperature, cooling rate, keeping last state or best state, add random restarts) and try to get the best result for a reasonable number of iterations.***\n",
    "\n",
    "You will need to add a counter and to balance the different parameters in a way that makes the average cost better without requiring much more computational power (i.e., same number of calls to the cost function, which in our case is equivalent to say, same number of calls to select)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ***OPTIONAL: implement a Tabu-search procedure.***\n",
    "\n",
    "Only use short-term memory. Compare the results to the above SimulatedAnnealing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
