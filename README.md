[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fsoliman%2Finf555/master?filepath=TD.ipynb)
(@mybinder.org)
<!--[![Binder](https://binderhub.fedcloud-tf.fedcloud.eu/badge.svg)](https://binderhub.fedcloud-tf.fedcloud.eu/v2/gh/soli/inf555/master?urlpath=lab/tree/td1/Australia.ipynb)
(@fedcloud.eu) and http://binder.pangeo.io/ https://notebooks.gesis.org/binder/ -->

Or run through Docker via (e.g. for the first TD)
`docker run -p 8080:8080 -p 8888:8888 -v "$PWD":/home/jovyan/work registry.gitlab.inria.fr/soliman/inf555/td1`
